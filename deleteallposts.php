<?php
require 'vendor/autoload.php';

use InstagramAPI\Instagram;
use InstagramAPI\Constants;
use InstagramAPI\Signatures;
use InstagramAPI\Exception;

use Medoo\Medoo;

$db = new Medoo([
    'database_type' => 'mysql',
    'database_name' => 'automateit',
    'server' => 'database',
    'username' => 'user',
    'password' => 'jayapura',
    'charset' => 'utf8',
]);

// Get batch to process
$username = 'miyazghayda';
if (isset($argv[1])) {
    $username = $argv[1];
}

$password = 'jayapura';
if (isset($argv[2])) {
    $password = $argv[2];
}

$userpk = '6045121554';
$rankToken = \InstagramAPI\Signatures::generateUUID();
// Starting at "null" means starting at the first page.
$maxId = null;

$ig = new Instagram(false, false);
try {
    $ig->login($username, $password);
    //$followers = $ig->people->getFollowers($userpk, $rankToken);
    //print_r($followers);
    //print_r($ig->people->getSelfInfo());
    $member_id = insertMember($db, $ig->people->getSelfInfo()->getUser());
    $maxId = null;
    $postCounter = 0;
    $mediaArr = [];
    do {
        //$response = $ig->timeline->getUserFeed($idolOnIg->getUser()->getPk(), $maxId);
        $response = $ig->timeline->getSelfUserFeed($maxId);
        $t = 1;
        foreach ($response->getItems() as $item) {
            //array_push($mediaArr, ['id' => $item->getPk(), 'type' => $item->getMediaType()])
            echo $postCounter . '. save post with caption ' . $item->getCaption()->getText() . PHP_EOL;
            // Insert Location
            $location_id = 1;
            if ($item->hasLocation() && $item->getLocation() !== null) {
                $location_id = insertLocation($db, $item->getLocation());
            }

            // Insert Post
            $post_id = insertPost($db, $item, $location_id, $member_id);

            // Insert Wad
            if ($post_id > 0 && $item->getMediaType() == 1) { // Photo
                insertWad($db, $item->getImageVersions2()->getCandidates()[0], $post_id, $item->getMediaType());
                echo $postCounter . '. Getting photo' . PHP_EOL;
                array_push($mediaArr, ['id' => $item->getPk(), 'type' => 'PHOTO']);
            } elseif ($post_id > 0 && $item->getMediaType() == 2) { // Video
                insertWad($db, $item->getVideoVersions()[0], $post_id, $item->getMediaType());
                echo $postCounter . '. Getting video' . PHP_EOL;
                array_push($mediaArr, ['id' => $item->getPk(), 'type' => 'VIDEO']);
            } elseif ($post_id > 0 && $item->getMediaType() == 8) { // Carousel
                insertWad($db, $item->getCarouselMedia(), $post_id, $item->getMediaType());
                echo $postCounter . '. Getting carousel' . PHP_EOL;
                array_push($mediaArr, ['id' => $item->getPk(), 'type' => 'ALBUM']);
            }

            $postCounter++;
        }// .foreach
        $maxId = $response->getNextMaxId();

        sleep(rand(12, 17));
    } while ($maxId !== null); // .loop for all idol feed(s)

    $i = 0;
    foreach ($mediaArr as $m) {
        echo $i . '. Deleting post with pk ' . $m['id'] . PHP_EOL;
        $i++;
        try {
            $ig->media->delete($m['id'], $m['type']);
        } catch (\Exception $eDelete) {
            echo $eDelete->getMessage() . PHP_EOL;
        }
        sleep(rand(12, 17));
    }
} catch (\Exception $e) {
    echo $e->getMessage() . PHP_EOL;
}

function insertMember($db = null, $datum = null)
{
    $check = $db->select('members',
        ['id'],
        ['pk' => $datum->getPk(), 'active' => true]
    );

    ($datum->hasProfilePicUrl()) ? $profpicurl = $datum->getProfilePicUrl() : $profpicurl = '';

    if (count($check) > 0) {
        $db->update('members',
            [
                'fullname' => $datum->getFullName(),
                'description' => $datum->getBiography(),
                'profpicurl' => $profpicurl,
                'followers' => $datum->getFollowerCount(),
                'followings' => $datum->getFollowingCount(),
                'contents' => $datum->getMediaCount(),
                'profpicurlfixed' => true,
                'modified' => date('Y-m-d H:i:s'),
            ],
            [
                'pk' => $datum->getPk(),
            ]
        );
        return $check[0]['id'];
    } else { // save member to db
        $db->insert('members',
            [
                'pk' => $datum->getPk(),
                'username' => $datum->getUsername(),
                'fullname' => $datum->getFullName(),
                'description' => $datum->getBiography(),
                'profpicurl' => $profpicurl,
                'followers' => $datum->getFollowerCount(),
                'followings' => $datum->getFollowingCount(),
                'contents' => $datum->getMediaCount(),
                'profpicurlfixed' => true,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ]
        );
        return $db->id();
    }
} // .function insertMember

function insertWad($db = null, $datum = null, $post_id = 1, $typeid = 1)
{
    $newData = [
        'post_id' => $post_id,
        'typeid' => $typeid,
        'sequence' => 0,
    ];

    if ($typeid == 1 || $typeid == 2) { // Single photo or video
        $check = $db->select('wads',
            ['id'],
            ['post_id' => $post_id, 'url' => $datum->getUrl(), 'active' => true]
        );

        // First, download from server
        /*$filePath = '/var/www/html/automateit2/kado/' . $post_id;
        if ($typeid == 1) {
            $filePath = $filePath . '.jpg';
        } elseif ($typeid == 2) {
            $filePath = $filePath . '.mp4';
        }
        
        set_time_limit(0);
        $fp = fopen ($filePath, 'w+');
        $ch = curl_init(str_replace(' ', '%20', $datum->getUrl()));
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_exec($ch);
        curl_close($ch);
        fclose($fp);*/

        if (count($check) < 1) {
            $newData['url'] = $datum->getUrl();
            $newData['width'] = $datum->getWidth();
            $newData['height'] = $datum->getHeight();
            $db->insert('wads', $newData);
            return $db->id();
        } else {
            return $check[0]['id'];
        }
    } elseif ($typeid == 8) { // Carousel
        $sequence = 0;
        foreach ($datum as $d) {
            $newData['url'] = '';
            $newData['width'] = 0;
            $newData['height'] = 0;
            $newData['typeid'] = $d->getMediaType();
            if ($d->getMediaType() == 1) { // Photo
                $reap = $d->getImageVersions2()->getCandidates()[0];
            } elseif ($d->getMediaType() == 2) { // Video
                $reap = $d->getVideoVersions()[0];
            }
            $check = $db->select('wads',
                ['id'],
                ['post_id' => $post_id, 'url' => $reap->getUrl(), 'active' => true]
            );

            if (count($check) < 1) {
                $newData['url'] = $reap->getUrl();
                $newData['width'] = $reap->getWidth();
                $newData['height'] = $reap->getHeight();
                $newData['sequence'] = $sequence;
                $sequence++;
                $db->insert('wads', $newData);
            }
        }
    }
} // .insertWad

function insertPost($db = null, $datum = null, $location_id = 1, $member_id = 1)
{
    $check = $db->select('posts',
        ['id'],
        ['pk' => $datum->getPk(), 'active' => true]
    );

    if (count($check) > 0) {
        $db->update('posts',
        ['member_id' => $member_id],
        ['pk' => $datum->getPk()]);
        return $check[0]['id'];
    } else { // save location to db
        ($datum->hasCaption() && $datum->getCaption() !== null) ? $caption = $datum->getCaption()->getText() : $caption = '';
        //echo "New post with caption $caption";

        $db->insert('posts',
            [
                'pk' => $datum->getPk(),
                'sourceid' => $datum->getId(),
                'location_id' => $location_id,
                'member_id' => $member_id,
                'typeid' => $datum->getMediaType(),
                'caption' => $caption,
                'likes' => $datum->getLikeCount(),
                'comments' => $datum->getCommentCount(),
                'takenat' => $datum->getTakenAt(),
            ]
        );
        return $db->id();
    }
} // .function insertPost

function insertLocation($db = null, $location = null)
{
    $check = $db->select('locations',
        ['id'],
        ['pk' => $location->getPk(), 'active' => true]
    );

    if (count($check) > 0) {
        return $check[0]['id'];
    } else { // save location to db
        //echo "New location named " . $location->getName() . "\n";
        ($location->hasFacebookPlacesId()) ? $fbplacesid = $location->getFacebookPlacesId() : $fbplacesid = 0;

        $db->insert('locations',
            [
                'pk' => $location->getPk(),
                'lat' => $location->getLat(),
                'lng' => $location->getLng(),
                'shortname' => $location->getShortName(),
                'name' => $location->getName(),
                'address' => $location->getAddress(),
                'fbplacesid' => $fbplacesid,

            ]
        );
        return $db->id();
    }
} // .function insertLocation
