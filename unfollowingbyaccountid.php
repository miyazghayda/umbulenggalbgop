<?php
require 'vendor/autoload.php';

use Medoo\Medoo;
use InstagramAPI\Instagram;
use InstagramAPI\Constants;
use InstagramAPI\Signatures;
use InstagramAPI\Exception;

$db = new Medoo([
    'database_type' => 'mysql',
    'database_name' => 'automateit',
    'server' => 'database',
    'username' => 'user',
    'password' => 'jayapura',
    'charset' => 'utf8'
]);

// Get batch to process
$usernametoprocess = 'miyazghayda';
if (isset($argv[1])) $usernametoprocess = $argv[1];
$maxunfollow = 350;
if (isset($argv[2])) $maxunfollow = $argv[2];

$members = $db->select('accounts',
    [
        '[<]proxies' => ['proxy_id' => 'id'],
        '[>]followinglists' => ['id' => 'account_id'],
        '[<]members' => ['followinglists.member_id' => 'id'],
    ],
    [
        'account' => [
            'accounts.id(account_id)',
            'accounts.user_id',
            'accounts.username(account_username)',
            'accounts.password'
        ],
        'proxy' => [
            'proxies.id AS proxy_id',
            'proxies.name AS proxy_name'
        ],
        'followinglists' => [
            'followinglists.member_id'
        ],
        'member' => [
            'members.id(member_id)',
            'members.username(member_username)',
            'members.pk',
        ],
    ],
    [
        'AND' => [
            'accounts.active' => true,
            'accounts.username' => $usernametoprocess,
            'followinglists.followed' => true,
            'followinglists.unfollowed' => false,
            'followinglists.who' => false,
            'followinglists.active' => true,
        ],
    ]
);

if (count($members) > 0) {
    // trying to login to ig
    $ig = new Instagram(false, false);
    try {
        if ($members[0]['proxy']['id'] > 1) $ig->setProxy($members[0]['proxy']['name']);
        $igLogin = $ig->login($members[0]['account']['account_username'], $members[0]['account']['password']);

        $i = 0;
        foreach ($members as $m) {
            if ($i < $maxunfollow) {
                try {
                    echo 'unfollowing ' . $m['member']['member_username'] . PHP_EOL;
                    $unfollow = $ig->people->unfollow($m['member']['pk']);
                    
                    if ($unfollow->getStatus() == 'ok') {
                        // update followinglists table
                        $db->update('followinglists',
                        ['unfollowed' => true, 'unfollowedat' => date('Y-m-d H:i:s'), 'who' => false, 'active' => true],
                        ['member_id' => $m['member']['member_id'], 'account_id' => $m['account']['account_id']]);
                        sleep(rand(28, 38));
                    }
                } catch (\Exception $unfollowExcept) {
                    // update followinglists table no matter what happen next
                    $db->update('followinglists',
                    ['who' => false, 'active' => false, 'note' => 'Error, no connection between'],
                    ['member_id' => $m['member']['member_id'], 'account_id' => $m['account']['account_id']]);

                    echo $unfollowExcept->getMessage() . PHP_EOL;
                }
            }
            $i++;
        }
    } catch (\Exception $loginExcept) {
        echo $loginExcept->getMessage() . PHP_EOL;
    }// .trying to login
}