<?php
function insertLocation($location = null) {
    $check = $this->Locations->find()
                  ->where(['pk' => $location->getPk(), 'active' => true]);

    if ($check->count() > 0) {
        $ret = $check->first();
        return $ret->id;
    } else {// save location to db
        $data = [];
        $data['pk'] = $location->getPk();
        $data['lat'] = $location->getLat();
        $data['lng'] = $location->getLng();
        $data['shortname'] = $location->getShortName();
        $data['name'] = $location->getName();
        $data['address'] = $location->getAddress();
        $data['active'] = true;
        $data['fbplacesid'] = 0;
        if ($location->hasFacebookPlacesId()) {
            $data['fbplacesid'] = $location->getFacebookPlacesId();
        }

        $location = $this->Locations->newEntity();
        $location = $this->Locations->patchEntity($location, $data);
        if ($this->Locations->save($location)) return $location->id;
    }
}// .function insertLocation
