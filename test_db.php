<?php
require 'vendor/autoload.php';

use Medoo\Medoo;

$database = new Medoo([
    'database_type' => 'mysql',
    'database_name' => 'automateit',
    'server' => 'localhost',
    'username' => 'user',
    'password' => 'jayapura',
    'charset' => 'utf8'
]);

$batch = 0;
if (isset($argv[1])) $batch = $argv[1];
// Select accounts where paid
//$data = $database->select('accounts', ['id', 'username', 'username'], ['active' => 1]);
//echo json_encode($data[0]['username']);
$data = $database->select('accounts',
    [
        '[>]preferences' => ['id' => 'account_id'],
        '[>]hashtaglists' => ['id' => 'account_id']
    ],
    [
        'accounts.id',
        'accounts.username',
        'accounts.password',
        'preferences.hashtagtofollowtoday',
        'hashtaglists.caption'
    ],
    [
        'AND' => [
            'accounts.active' => true,
            'preferences.gethashtagtofollowtoday' => true,
            'hashtaglists.typeid' => 1,
            'hashtaglists.whitelist' => true,
            'hashtaglists.active' => true
        ],
        'ORDER' => ['accounts.id' => 'DESC'],
        'LIMIT' => 2
    ]
);
echo json_encode($data);
