<?php
require 'vendor/autoload.php';

use Medoo\Medoo;
use InstagramAPI\Instagram;
use InstagramAPI\Constants;
use InstagramAPI\Signatures;
use InstagramAPI\Exception;

require_once 'db.php';
/*$db = new Medoo([
    'database_type' => 'mysql',
    'database_name' => 'automateit',
    'server' => 'database',
    'username' => 'user',
    'password' => 'jayapura',
    'charset' => 'utf8'
]);*/

// Get batch to process
$batch = 0;
if (isset($argv[1])) $batch = $argv[1];

while (true) {
    $ig = new Instagram(false, false);

    echo "File \033[34m" . basename(__FILE__, '.php') . "\033[0m Waiting for account(s) to process on " . date('d-m-Y H:i') . PHP_EOL;

    $accounts = $db->select('accounts',
        [
            '[>]preferences' => ['id' => 'account_id'],
            '[<]proxies' => ['proxy_id' => 'id']
        ],
        [
            'account' => [
                'accounts.id(account_id)',
                'accounts.user_id',
                'accounts.username',
                'accounts.password'
            ],
            'preference' => [
                'preferences.maxfollowperday',
                'preferences.hashtagtofollowtoday',
                'preferences.gethashtagtofollowtoday',
            ],
            'proxy' => [
                'proxies.id AS proxy_id',
                'proxies.name AS proxy_name'
            ]
        ],
        [
            'AND' => [
                'accounts.statusid' => 5,
                'accounts.active' => true,
                'preferences.gethashtagtofollowtoday' => true,
            ]
        ]
    );
    foreach ($accounts as $a) {
        $accountmaxfollow = $a['preference']['maxfollowperday'];
        $accounthadfollowingfor = $a['preference']['hashtagtofollowtoday'];

        $hashtags = $db->select('hashtaglists',
            ['caption'],
            [
                'account_id' => $a['account']['account_id'],
                'typeid' => 1,
                'whitelist' => true,
                'active' => true
            ]
        );
        if (count($hashtags) > 0) {
            // Login to IG
            try {
                if ($a['proxy']['id'] > 1) $ig->setProxy($a['proxy']['name']);
                $igLogin = $ig->login($a['account']['username'], $a['account']['password']);
                //if ($igLogin->getStatus() == 'ok') {
                echo "Succeed to login to " . $a['account']['username'] . "\n";
                $i = 0;
                foreach ($hashtags as $h) {
                    $rankToken = Signatures::generateUUID();
                    echo "Now trying to get feed from #" . $h['caption']. "\n";
                    $feed = $ig->hashtag->getFeed($h['caption'], $rankToken, null);
                    if ($feed->getStatus() == 'ok') {
                        foreach ($feed->getItems() as $item) {
                            // Insert Location
                            $location_id = 1;
                            if ($item->hasLocation() && $item->getLocation() !== null) {
                                $location_id = insertLocation($db, $item->getLocation());
                            }

                            // Insert Member
                            $member_id = insertMember($db, $item->getUser());

                            // Insert Post
                            $post_id = insertPost($db, $item, $location_id, $member_id);

                            // Insert Wad
                            if ($post_id > 0 && $item->getMediaType() == 1) {// Photo
                                insertWad($db, $item->getImageVersions2()->getCandidates()[0], $post_id, $item->getMediaType());
                            } elseif ($post_id > 0 && $item->getMediaType() == 2) {// Video
                                insertWad($db, $item->getVideoVersions()[0], $post_id, $item->getMediaType());
                            } elseif ($post_id > 0 && $item->getMediaType() == 8) {// Carousel
                                insertWad($db, $item->getCarouselMedia(), $post_id, $item->getMediaType());
                            }

                            // Follow
                            if (($accounthadfollowingfor + $i) < $accountmaxfollow && notYetFollowed($db, $a['account']['account_id'], $member_id)) {
                                try {
                                    $following = $ig->people->follow($item->getUser()->getPk());
                                    if ($following->getStatus() == 'ok') {
                                        insertFollowinglist($db, $a['account']['account_id'], $member_id);
                                        echo $i . ". Following " . $item->getUser()->getUsername() . " success\n";
                                        $i++;
                                    }
                                    sleep(rand(28, 38));
                                } catch (\Exception $ex) {
                                    echo $ex->getMessage() . PHP_EOL;
                                }// .try following
                            }// .if following below maxfollowperday and not yet followed
                        }// .foreach feed item
                    }// .if feed status is ok
                }// .foreach hashtags
                // Update preferences table
                $db->update('preferences', ['hashtagtofollowtoday' => $accounthadfollowingfor + $i], ['account_id' => $a['account']['account_id']]);
                if (($accounthadfollowingfor + $i) >= $accountmaxfollow) {
                    $db->update('preferences', ['gethashtagtofollowtoday' => false], ['account_id' => $a['account']['account_id']]);
                }
                //}
            } catch (\Exception $e) {
                echo $e->getMessage() . PHP_EOL;
            }// .try login ig
        }// .if count hashtag > 0
    }// .foreach accounts

    // Sleep foreach process
    sleep(1800);
}

function notYetFollowed($db = null, $account_id = 1, $member_id = 1) {
    $check = $db->select('followinglists',
        ['id'],
        ['fellow_id' => $member_id, 'account_id' => $account_id, 'active' => true]
    );

    if (count($check) > 0) {
        return false;
    } else {
        return true;
    }
}

function insertFollowinglist($db = null, $account_id = 1, $member_id = 1) {
    $check = $db->select('followinglists',
        ['id'],
        ['fellow_id' => $member_id, 'account_id' => $account_id, 'active' => true]
    );

    if (count($check) > 0) {
        return $check['id'];
    } else {// save location to db
        $db->insert('followinglists', 
            [
                'member_id' => $member_id,
                'fellow_id' => $member_id,
                'typeid' => 2,
                'followed' => true,
                'followedat' => date('Y-m-d H:i:s'),
                'account_id' => $account_id,
                'note' => ''
            ]
        );
        return $db->id();
    }
}// .insertWad


function insertWad($db = null, $datum = null, $post_id = 1, $typeid = 1) {
    $newData = [
        'post_id' => $post_id,
        'typeid' => $typeid,
        'sequence' => 0
    ];

    if ($typeid == 1 || $typeid == 2) {// Single photo or video
        $newData['url'] = $datum->getUrl();
        $newData['width'] = $datum->getWidth();
        $newData['height'] = $datum->getHeight();
        $db->insert('wads', $newData);
        return $db->id();
    } elseif ($typeid == 8) {// Carousel
        $sequence = 0;
        foreach ($datum as $d) {
            $newData['url'] = '';
            $newData['width'] = 0;
            $newData['height'] = 0;
            $newData['typeid'] = $d->getMediaType();
            if ($d->getMediaType() == 1) {// Photo
                $reap = $d->getImageVersions2()->getCandidates()[0];
            } elseif ($d->getMediaType() == 2) {// Video
                $reap = $d->getVideoVersions()[0];
            }
            $newData['url'] = $reap->getUrl();
            $newData['width'] = $reap->getWidth();
            $newData['height'] = $reap->getHeight();
            $newData['sequence'] = $sequence;
            $sequence++;
            $db->insert('wads', $newData);
        }
    }
}// .insertWad

function insertPost($db = null, $datum = null, $location_id = 1, $member_id = 1) {
    $check = $db->select('posts',
        ['id'],
        ['pk' => $datum->getPk(), 'active' => true]
    );

    if (count($check) > 0) {
        return $check[0]['id'];
    } else {// save location to db
        ($datum->hasCaption() && $datum->getCaption() !== null) ? $caption = $datum->getCaption()->getText() : $caption = '';
        //echo "New post with caption $caption";

        $db->insert('posts', 
            [
                'pk' => $datum->getPk(),
                'sourceid' => $datum->getId(),
                'location_id' => $location_id,
                'member_id' => $member_id,
                'typeid' => $datum->getMediaType(),
                'caption' => $caption,
                'likes' => $datum->getLikeCount(),
                'comments' => $datum->getCommentCount(),
                'takenat' => $datum->getTakenAt()
            ]
        );
        return $db->id();
    }
}// .function insertPost

function insertMember($db = null, $datum = null) {
    $check = $db->select('members',
        ['id'],
        ['pk' => $datum->getPk(), 'active' => true]
    );

    if (count($check) > 0) {
        return $check[0]['id'];
    } else {// save location to db
        //echo "New member named " . $datum->getUsername() . "\n";
        ($datum->hasProfilePicUrl()) ? $profpicurl = $datum->getProfilePicUrl() : $profpicurl = '';

        $db->insert('members', 
            [
                'pk' => $datum->getPk(),
                'username' => $datum->getUsername(),
                'fullname' => $datum->getFullName(),
                'description' => $datum->getBiography(),
                'profpicurl' => $profpicurl,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ]
        );
        return $db->id();
    }
}// .function insertMember

function insertLocation($db = null, $location = null) {
    $check = $db->select('locations',
        ['id'],
        ['pk' => $location->getPk(), 'active' => true]
    );

    if (count($check) > 0) {
        return $check[0]['id'];
    } else {// save location to db
        //echo "New location named " . $location->getName() . "\n";
        ($location->hasFacebookPlacesId()) ? $fbplacesid = $location->getFacebookPlacesId() : $fbplacesid = 0;

        $db->insert('locations', 
            [
                'pk' => $location->getPk(),
                'lat' => $location->getLat(),
                'lng' => $location->getLng(),
                'shortname' => $location->getShortName(),
                'name' => $location->getName(),
                'address' => $location->getAddress(),
                'fbplacesid' => $fbplacesid

            ]
        );
        return $db->id();
    }
}// .function insertLocation
