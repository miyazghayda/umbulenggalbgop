<?php
require 'vendor/autoload.php';

use Medoo\Medoo;
use InstagramAPI\Instagram;
use InstagramAPI\Constants;
use InstagramAPI\Signatures;
use InstagramAPI\Exception;

require_once 'db.php';
/*$db = new Medoo([
    'database_type' => 'mysql',
    'database_name' => 'automateit',
    'server' => 'database',
    'username' => 'user',
    'password' => 'jayapura',
    'charset' => 'utf8'
]);*/

// Get batch to process
$batch = 0;
if (isset($argv[1])) $batch = $argv[1];

while (true) {
    $ig = new Instagram(false, false);

    echo "File \033[34m" . basename(__FILE__, '.php') . "\033[0m Waiting for accounts to process on " . date('d-m-Y H:i') . PHP_EOL;

    $accounts = $db->select('accounts',
        [
            '[<]proxies' => ['proxy_id' => 'id'],
        ],
        [
            'account' => [
                'accounts.id(account_id)',
                'accounts.proxy_id',
                'accounts.username',
                'accounts.password',
            ],
            'proxy' => [
                'proxies.id(proxy_id)',
                'proxies.name(proxy_name)',
            ],
        ],
        [
            'AND' => [
                'accounts.statusid' => 5,
                'accounts.active' => true
            ]
        ]
    );

    //print_r($accounts);
    foreach ($accounts as $account) {
        try {
            if ($account['account']['proxy_id'] > 1) $ig->setProxy($account['proxy']['proxy_name']);
            echo 'Login with username ' . $account['account']['username'] . PHP_EOL;
            // Login akun IG
            $ig->login($account['account']['username'], $account['account']['password']);
        } catch (\Exception $e) {
            echo $e . PHP_EOL;
        }// .try login

        // get self info
        $ownInfo = $ig->people->getSelfInfo();
        $member_id = insertMember($db, $ownInfo->getUser());
        updateAccount($db, $account['account']['account_id'], $ownInfo->getUser());

        // now trying to get self followers
        try {
            $rankToken = \InstagramAPI\Signatures::generateUUID();
            $maxId = null;

            // update current follower data so system know when someone is unfollowing us
            $db->update('vassals', 
                ['modified' => date('Y-m-d H:i:s'), 'active' => false],
                ['member_id' => $member_id]);

            //echo 'Now trying to gather ' . $celebrity['member']['member_username'] . ' followers' . PHP_EOL;
            echo 'Now trying to gather own followers' . PHP_EOL;
            $i = 0;
            do {
                $response = $ig->people->getSelfFollowers($rankToken, null, $maxId);
                if ($response->getStatus() === 'ok') {
                    foreach ($response->getUsers() as $u) {
                        // Insert Member
                        $fellow_id = insertMember($db, $u);

                        // Insert Vassal
                        $vassal_id = insertVassal($db, $member_id, $fellow_id);

                        echo $i . '. saving ' . $u->getUsername() . PHP_EOL;
                        $i++;
                    }
                }

                $maxId = $response->getNextMaxId();
                // comment this on production
                //$maxId = null;
                sleep(rand(7, 12));
            } while ($maxId !== null);
        } catch (\Exception $e) {
            echo $e->getMessage() . PHP_EOL;
        }// .try gather followers

        sleep(rand(7, 12));
    }
    // sleep for a day
    sleep(86400);
}// .while true

function updateAccount($db = null, $account_id = 1, $datum = null) {
    $check = $db->select('accounts',
    ['id'],
    ['id' => $account_id]);

    if (count($check) > 0) {
        ($datum->hasProfilePicUrl()) ? $profpicurl = $datum->getProfilePicUrl() : $profpicurl = '';
        $db->update('accounts',
            [
                'contents' => $datum->getMediaCount(),
                'followers' => $datum->getFollowerCount(),
                'followings' => $datum->getFollowingCount(),
                'closed' => $datum->getIsPrivate(),
                'modified' => date('Y-m-d H:i:s'),
                'profpicurl' => $profpicurl,
                'fullname' => $datum->getFullName(),
                'description' => $datum->getBiography(),
                'pk' => $datum->getPk(),
                'profpicurlfixed' => true,
            ],
            ['id' => $account_id]);
    }
}

function insertVassal($db = null, $member_id = 1, $fellow_id = 1) {
    $check = $db->select('vassals',
        ['id'],
        ['member_id' => $member_id, 'fellow_id' => $fellow_id]
    );

    if (count($check) > 0) {
        $db->update('vassals',
            ['modified' => date('Y-m-d H:i:s'), 'active' => true],
            ['id' => $check[0]['id']]);
        return $check[0]['id'];
    } else {// save vassal (idol follower) to db
        $db->insert('vassals',[
            'member_id' => $member_id,
            'fellow_id' => $fellow_id,
            'created' => date('Y-m-d H:i:s'),
            'modified' => date('Y-m-d H:i:s'),
        ]);
        return $db->id();
    }
}// .function insertVassal

function insertMember($db = null, $datum = null) {
    $check = $db->select('members',
        ['id'],
        ['pk' => $datum->getPk(), 'active' => true]
    );

    if (count($check) > 0) {
        if ($datum->hasFollowerCount()) {
            $db->update('members',
                [
                    'contents' => $datum->getMediaCount(),
                    'followers' => $datum->getFollowerCount(),
                    'followings' => $datum->getFollowingCount(),
                    'closed' => $datum->getIsPrivate(),
                    'modified' => date('Y-m-d H:i:s'),
                    'profpicurlfixed' => true,
                ],
                ['id' => $check[0]['id']]);

        } else {
            $db->update('members',
                [
                    'closed' => $datum->getIsPrivate(),
                    'modified' => date('Y-m-d H:i:s'),
                    'profpicurlfixed' => true,
                ],
                ['id' => $check[0]['id']]);
        }
        return $check[0]['id'];
    } else {// save member to db
        //echo "New member named " . $datum->getUsername() . "\n";
        ($datum->hasProfilePicUrl()) ? $profpicurl = $datum->getProfilePicUrl() : $profpicurl = '';

        if ($datum->hasFollowerCount()) {
            $db->insert('members', 
                [
                    'pk' => $datum->getPk(),
                    'username' => $datum->getUsername(),
                    'fullname' => $datum->getFullName(),
                    'profpicurl' => $profpicurl,
                    'contents' => $datum->getMediaCount(),
                    'followers' => $datum->getFollowerCount(),
                    'followings' => $datum->getFollowingCount(),
                    'closed' => $datum->getIsPrivate(),
                    'created' => date('Y-m-d H:i:s'),
                    'modified' => date('Y-m-d H:i:s'),
                    'profpicurlfixed' => true,
                ]
            );
        } else {
            $db->insert('members', 
                [
                    'pk' => $datum->getPk(),
                    'username' => $datum->getUsername(),
                    'fullname' => $datum->getFullName(),
                    'profpicurl' => $profpicurl,
                    'closed' => $datum->getIsPrivate(),
                    'created' => date('Y-m-d H:i:s'),
                    'modified' => date('Y-m-d H:i:s'),
                    'profpicurlfixed' => true,
                ]
            );
        }
        return $db->id();
    }
}// .function insertMember