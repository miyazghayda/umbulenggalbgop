<?php
require 'vendor/autoload.php';

use Medoo\Medoo;

require_once 'db.php';
/*$db = new Medoo([
    'database_type' => 'mysql',
    'database_name' => 'automateit',
    'server' => 'database',
    'username' => 'user',
    'password' => 'jayapura',
    'charset' => 'utf8'
]);*/

$basePath = '/var/www/html/automateit/webroot/files/csv/celebrities/';

while (true) {
    echo "File \033[34m" . basename(__FILE__, '.php') . "\033[0m Waiting for celebrit(y)ies to process on " . date('d-m-Y H:i') . PHP_EOL;
    $celebrities = $db->select('celebrities',
        [
            '[<]members' => ['member_id' => 'id'],
        ],
        [
            'celebrity' => [
                'celebrities.id(celebrity_id)',
                'celebrities.followers(celebrity_followers)',
                'celebrities.followersaved(celebrity_followersaved)',
            ],
            'member' => [
                'members.id(member_id)',
                'members.pk(pk)',
                'members.username(username)',
                'members.fullname(fullname)',
                'members.description(description)',
                'members.followings(followings)',
                'members.followers(followers)',
                'members.contents(contents)',
                'members.closed(closed)',
            ],
        ],
        [
            'AND' => [
                'celebrities.id[!]' => 1,
                'celebrities.active' => true
            ]
        ]
    );
   
    foreach ($celebrities as $celebrity) {
        $fullPath = $basePath . trim($celebrity['member']['username']) . '.csv';
        
        $fellowsCount = $db->count('vassals',
            [
                'AND' => [
                    'vassals.member_id' => $celebrity['member']['member_id'],
                    'vassals.active' => true,
                ]
            ]
        );

        // check if file exists
        $fileExists = file_exists($fullPath);
        // if no file, create
        if ($fellowsCount > 0) {
            if (!$fileExists) {
                echo 'create file ' . $fullPath . PHP_EOL;
                createCsv($db, $celebrity, $fullPath, $fellowsCount);
            } else {
                // read csv row
                $csvCount = csvRowCount($fullPath);
                echo 'csv: ' . $csvCount . PHP_EOL;
                echo 'db: ' . $fellowsCount . PHP_EOL;

                if (($csvCount - 1) !== $fellowsCount && ($csvCount - 1) < $fellowsCount) {
                    echo 'create file ' . $fullPath . PHP_EOL;
                    createCsv($db, $celebrity, $fullPath, $fellowsCount);
                }
            }// .if file exists
        }// .if fellow count > 0
    }// .foreach celebrities

    // sleep for a day
    sleep(86400);
}// .while true

function csvRowCount($fullPath = null) {
    $fp = file($fullPath, FILE_SKIP_EMPTY_LINES);
    return count($fp);
}// .csvRowCount

function createCsv($db = null, $member = null, $fullPath = null, $fellowsCount = 0) {
    $data = [];
    $limit = 5000;
    $start = 0;
    do {
        echo $member['member']['username'] . ' load follower(s) data ' . $start . '/' . $fellowsCount . PHP_EOL;
        $fellows = $db->select('vassals',
            [
                '[<]fellows' => ['fellow_id' => 'id'],
            ],
            [
                'fellow' => [
                    'fellows.id(fellow_id)',
                    'fellows.pk(pk)',
                    'fellows.username(username)',
                    'fellows.fullname(fullname)',
                    'fellows.description(description)',
                    'fellows.followings(followings)',
                    'fellows.followers(followers)',
                    'fellows.contents(contents)',
                    'fellows.closed(closed)',
                ],
            ],
            [
                'AND' => [
                    'vassals.member_id' => $member['member']['member_id'],
                    'vassals.active' => true,
                ],
                'ORDER' => [
                    'fellows.username' => 'ASC'
                ],
                'LIMIT' => [
                    $start,
                    $limit
                ]
            ]
        );


       $handle = fopen($fullPath, 'a');
       if ($start == 0) {
            fputcsv($handle, [
                'No',
                'ID',
                'Username',
                'Fullname',
                'Private',
                'Link'],
                ';'
            );
        }

        $i = $start + 1;
        foreach ($fellows as $fellow) {
            $fellow['fellow']['closed'] ? $closed = 'YA' : $closed = 'TIDAK';
            $fellowFullname = preg_replace('/\s+/', ' ', $fellow['fellow']['fullname']);

            fputcsv($handle,[
                $start,
                $fellow['fellow']['pk'],
                $fellow['fellow']['username'],
                $fellowFullname,
                $closed,
                'https://www.instagram.com/' . $fellow['fellow']['username'],
            ],';');
            $i++;
        }
        fclose($handle);
        $start = $start + $limit;
    } while ($start < $fellowsCount);
    
    //if (count($fellows) > 0) {
    $memberFullname = preg_replace('/\s+/', ' ', $member['member']['fullname']);
    $member['member']['closed'] ? $closed = 'YA' : $closed = 'TIDAK';
}// .writeCsv