<?php
require 'vendor/autoload.php';

use Medoo\Medoo;
use InstagramAPI\Instagram;
use InstagramAPI\Constants;
use InstagramAPI\Signatures;
use InstagramAPI\Exception;
use InstagramAPI\Media\Photo\InstagramPhoto;
use InstagramAPI\Media\Video\InstagramVideo;

$db = new Medoo([
    'database_type' => 'mysql',
    'database_name' => 'automateit',
    'server' => 'localhost',
    'username' => 'user',
    'password' => 'jayapura',
    'charset' => 'utf8'
]);

$reap_path = join(DIRECTORY_SEPARATOR, [
    '', 'var', 'www', 'html', 'automateit',
    'webroot', 'files', 'images', 'upload', '']);

$wad_path = join(DIRECTORY_SEPARATOR, [
    '', 'var', 'www', 'html', 'automateit2',
    'files', 'images', 'upload', '']);

while (true) {
    echo "Waiting for account(s) to process on " . date('d-m-Y H:i') . "\n";
    // Gather all account_id(s) on both cargos and replicas tables
    $accountsOnCargo = $db->select('cargos',
    ['account_id'],
    [
        'AND' => [
            'schedule[<=]' => date('Y-m-d H:i'),
            'uploaded' => false,
            'active' => true
        ],
        'GROUP' => ['account_id']
    ]);
    $accountsOnReplica = $db->select('replicas',
    ['account_id'],
    [
        'AND' => [
            'schedule[<=]' => date('Y-m-d H:i'),
            'uploaded' => false,
            'active' => true
        ],
        'GROUP' => ['account_id']
    ]);

    // Remove redundant account_id
    $accountIds = [];
    foreach ($accountsOnCargo as $a) {
        if (!in_array($a['account_id'], $accountIds)) array_push($accountIds, $a['account_id']);
    }
    foreach ($accountsOnReplica as $a) {
        if (!in_array($a['account_id'], $accountIds)) array_push($accountIds, $a['account_id']);
    }

    // Check if account_id is paid account
    $paidAccountIds = [];
    foreach ($accountIds as $a) {
        $check = $db->count('accounts',
        [
            'AND' => [
                'id' => $a,
                'statusid' => 5,
                'active' => true,
            ]
        ]);
        if ($check > 0) {
            array_push($paidAccountIds, $a);
        }
    }

    if (count($paidAccountIds) > 0) {
        $ig = new Instagram(false, false);
        foreach ($paidAccountIds as $account_id) {
            $account = $db->select('accounts',
                ['[<]proxies' => ['proxy_id' => 'id']],
                [
                    'account' => [
                        'accounts.id(account_id)',
                        'accounts.user_id',
                        'accounts.username',
                        'accounts.password',
                        'accounts.proxy_id'
                    ],
                    'proxy' => [
                        'proxies.name AS proxy_name',
                    ]
                ],
                [
                    'AND' => [
                        'accounts.id' => $account_id
                    ],
                    'LIMIT' => 1
            ]);
            $account = $account[0];
            
            try {
                if ($account['account']['proxy_id'] > 1) $ig->setProxy($account['account']['proxy']['name']);
                echo 'Login with username ' . $account['account']['username'] . PHP_EOL;
                // Login akun IG
                $ig->login($account['account']['username'], $account['account']['password']);
                
                // Process cargos table, original content from account
                $cargos = $db->select('cargos',
                    [
                        'cargos.id(cargo_id)',
                        'cargos.schedule',
                    ],
                    [
                        'AND' => [
                            'schedule[<=]' => date('Y-m-d H:i'),
                            'uploaded' => false,
                            'active' => true,
                        ],
                        'ORDER' => ['schedule' => 'ASC'],
                    ]
                );
                
                // Get contents (reaps) and upload to IG
                foreach ($cargos as $c) {
                    //uploadReap($db, $ig, $reap_path, $account, $c);
                    uploadData($db, $ig, 1, $reap_path, $account, $c);
                }

                // Process replicas table, repost content from other account(s)
                $replicas = $db->select('replicas',
                    [
                        'id(replica_id)',
                        'schedule',
                        'caption',
                        'post_id',
                    ],
                    [
                        'AND' => [
                            'schedule[<=]' => date('Y-m-d H:i'),
                            'uploaded' => false,
                            'active' => true,
                        ],
                        'ORDER' => ['schedule' => 'ASC']
                    ]);

                    // Get contents (wads) and upload to IG
                    foreach ($replicas as $r) {
                        uploadData($db, $ig, 2, $wad_path, $account, $r);
                        //uploadWad($db, $ig, $wad_path, $account, $r);
                    }
            } catch (Exception $loginException) {
                echo $loginException->getMessage() . PHP_EOL;
            }// .try login

        }
    }
    sleep(60);
}// .while true

// $reapOrWad
// 1 = reap
// 2 = wad
function uploadData($db = null, $ig = null, $reapOrWad = 1, $path = null, $account = null, $data = null) {
    if ($reapOrWad == 1) {
        $contents = $db->select('reaps', 
        [
            'id(reap_id)',
            'typeid',
            'extension',
            'sequence'
        ],
        [
            'AND' => [
                'cargo_id' => $data['cargo_id'],
                'active' => true
            ],
            'ORDER' => ['sequence' => 'ASC']
        ]);
    } elseif ($reapOrWad == 2) {
        $contents = $db->select('wads',
            [
                'id(wad_id)',
                'typeid',
                'url',
                'sequence'
            ],
            [
                'AND' => [
                    'post_id' => $data['post_id'],
                    'active' => true,
                ],
                'ORDER' => ['sequence' => 'ASC']
            ]);
    }

    // Single content
    if (count($contents) == 1) {
        echo 'Upload content with caption ' . $data['caption'] . PHP_EOL;
        try {
            if ($reapOrWad == 1) {
                $content = new InstagramPhoto($path . $contents[0]['reap_id'] . '.' . $contents[0]['extension']);
            } elseif ($reapOrWad == 2) {
                // First, download from server
                $filePath = $path . $contents[0]['wad_id'];
                if ($contents[0]['typeid'] == 1) {
                    $filePath = $filePath . '.jpg';
                } elseif ($contents[0]['typeid'] == 2) {
                    $filePath = $filePath . '.mp4';
                }
                
                set_time_limit(0);
                $fp = fopen ($filePath, 'w+');
                $ch = curl_init(str_replace(' ', '%20', $contents[0]['url']));
                curl_setopt($ch, CURLOPT_FILE, $fp);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                curl_exec($ch);
                curl_close($ch);
                fclose($fp);
                $content = new InstagramPhoto($filePath);
            }
            $uploading = $ig->timeline->uploadPhoto($content->getFile(), ['caption' => $data['caption']]);
            if ($uploading->getStatus() == 'ok') {
                if ($reapOrWad == 1) {
                    $db->update('cargos',
                        ['uploaded' => true, 'modified' => date('Y-m-d H:i:s')],
                        ['id' => $data['cargo_id']]);
                } elseif ($reapOrWad == 2) {
                    $db->update('replicas',
                        ['uploaded' => true, 'modified' => date('Y-m-d H:i:s')],
                        ['id' => $data['replica_id']]);
                }
                return true;
            }
            echo 'Fail to upload' . PHP_EOL;
        } catch (Exception $uploadException) {
            echo $uploadException->getMessage() . PHP_EOL;
        }// .try upload content
    // Multiple contents
    } elseif (count($contents) > 1) {
        echo 'Upload album with caption ' . $data['caption'] . PHP_EOL;
       
        // Preparing album to upload
        $media = [];
        if ($reapOrWad == 1) {
            foreach($contents as $content) {
                if ($content['typeid'] == 1) array_push($media, ['type' => 'photo', 'file' => $path . $content['reap_id'] . '.' . $content['extension']]);
                if ($content['typeid'] == 2) array_push($media, ['type' => 'video', 'file' => $path . $content['reap_id'] . '.' . $content['extension']]);
            }
        } elseif ($reapOrWad == 2) {
            foreach ($contents as $content) {
                // First, download from server
                $filePath = $path . $content['wad_id'];
                if ($content['typeid'] == 1) {
                    $filePath = $filePath . '.jpg';
                } elseif ($content['typeid'] == 2) {
                    $filePath = $filePath . '.mp4';
                }

                set_time_limit(0);
                $fp = fopen ($filePath, 'w+');
                $ch = curl_init(str_replace(' ', '%20', $content['url']));
                curl_setopt($ch, CURLOPT_FILE, $fp);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                curl_exec($ch);
                curl_close($ch);
                fclose($fp);
                if ($content['typeid'] == 1) array_push($media, ['type' => 'photo', 'file' => $filePath]);
                if ($content['typeid'] == 2) array_push($media, ['type' => 'video', 'file' => $filePath]);
            }// .foreach contents
        }

        $mediaOptions = ['targetFeed' => Constants::FEED_TIMELINE_ALBUM];

        foreach($media as &$item) {
            $validMedia = null;
            switch ($item['type']) {
            case 'photo':
                $validMedia = new InstagramPhoto($item['file'], $mediaOptions);
                break;
            case 'video':
                $validMedia = new InstagramVideo($item['file'], $mediaOptions);
                break;
            default:

            }
            if ($validMedia === null) {
                continue;
            }

            try {
                $item['file'] = $validMedia->getFile();
                $item['__media'] = $validMedia;
            } catch(Exception $e) {
                continue;
            }
            if (!isset($mediaOptions['forceAspectRatio'])) {
                $mediaDetails = $validMedia instanceof InstagramPhoto
                    ? new \InstagramAPI\Media\Photo\PhotoDetails($item['file'])
                    : new \InstagramAPI\Media\Video\VideoDetails($item['file']);
                $mediaOptions['forceAspectRatio'] = $mediaDetails->getAspectRatio();
            }
        }// .foreach media

        // Upload album
        try {
            $uploading = $ig->timeline->uploadAlbum($media, ['caption' => $data['caption']]);
            if ($uploading->getStatus() == 'ok') {
                if ($reapOrWad == 1) {
                $db->update('cargos',
                    ['uploaded' => true, 'modified' => date('Y-m-d H:i:s')],
                    ['id' => $cargo['cargo']['cargo_id']]);
                } elseif ($reapOrWad == 2) {
                    $db->update('replicas',
                        ['uploaded' => true, 'modified' => date('Y-m-d H:i:s')],
                        ['id' => $data['replica_id']]);
                }
                return true;
            }
        } catch (Exception $uploadException) {
            echo $uploadException->getMessage() . PHP_EOL;
        }// .try upload contents
    }
}

function uploadWad($db = null, $ig = null, $wad_path = null, $account = null, $replica = null) {
    /*$wads = $db->select('wads',
    [
        'id(wad_id)',
        'typeid',
        'url',
        'sequence'
    ],
    [
        'AND' => [
            'post_id' => $replica['post_id'],
            'active' => true,
        ],
        'ORDER' => ['sequence' => 'ASC']*/

        // Single content
        if (count($wads) == 1) {
            echo 'Upload content with caption ' . $replica['replica']['caption'] . PHP_EOL;

        // Multiple contents
        } elseif (count($wads) > 1) {
            echo 'Upload album with caption ' . $replica['replica']['caption'] . PHP_EOL;
        }
}

function uploadReap($db = null, $ig = null, $reap_path = null, $account = null, $cargo = null) {
    $reaps = $db->select('reaps', 
    [
        'id(reap_id)',
        'typeid',
        'extension',
        'sequence'
    ],
    [
        'AND' => [
            'cargo_id' => $cargo['cargo']['cargo_id'],
            'active' => true
        ],
        'ORDER' => ['sequence' => 'ASC']
    ]);

    // Single content
    if (count($reaps) == 1) {
        echo 'Upload content with caption ' . $cargo['cargo']['caption'] . PHP_EOL;
        try {
            $content = new InstagramPhoto($reap_path . $reaps[0]['reap_id'] . '.' . $reaps[0]['extension']);
            $uploading = $ig->timeline->uploadPhoto($content->getFile(), ['caption' => $cargo['cargo']['caption']]);
            if ($uploading->getStatus() == 'ok') {
                $db->update('cargos',
                    ['uploaded' => true, 'modified' => date('Y-m-d H:i:s')],
                    ['id' => $cargo['cargo']['cargo_id']]);
                return true;
            }
            echo 'Fail to upload' . PHP_EOL;
        } catch (Exception $uploadException) {
            echo $uploadException->getMessage() . PHP_EOL;
        }// .try upload content
    // Multiple contents
    } elseif (count($reaps) > 1) {
        echo 'Upload album with caption ' . $cargo['cargo']['caption'] . PHP_EOL;
       
        // Preparing album to upload
        $media = [];
        foreach($reaps as $reap) {
            if ($reap['typeid'] == 1) array_push($media, ['type' => 'photo', 'file' => $reap_path . $reap['reap_id'] . '.' . $reap['extension']]);
            if ($reap['typeid'] == 2) array_push($media, ['type' => 'video', 'file' => $reap_path . $reap['reap_id'] . '.' . $reap['extension']]);
        }
        $mediaOptions = ['targetFeed' => Constants::FEED_TIMELINE_ALBUM];

        foreach($media as &$item) {
            $validMedia = null;
            switch ($item['type']) {
            case 'photo':
                $validMedia = new InstagramPhoto($item['file'], $mediaOptions);
                break;
            case 'video':
                $validMedia = new InstagramVideo($item['file'], $mediaOptions);
                break;
            default:

            }
            if ($validMedia === null) {
                continue;
            }

            try {
                $item['file'] = $validMedia->getFile();
                $item['__media'] = $validMedia;
            } catch(Exception $e) {
                continue;
            }
            if (!isset($mediaOptions['forceAspectRatio'])) {
                $mediaDetails = $validMedia instanceof InstagramPhoto
                    ? new \InstagramAPI\Media\Photo\PhotoDetails($item['file'])
                    : new \InstagramAPI\Media\Video\VideoDetails($item['file']);
                $mediaOptions['forceAspectRatio'] = $mediaDetails->getAspectRatio();
            }
        }// .foreach media

        // Upload album
        try {
            $uploading = $ig->timeline->uploadAlbum($media, ['caption' => $cargo['cargo']['caption']]);
            if ($uploading->getStatus() == 'ok') {
                $db->update('cargos',
                    ['uploaded' => true, 'modified' => date('Y-m-d H:i:s')],
                    ['id' => $cargo['cargo']['cargo_id']]);
                return true;
            }
        } catch (Exception $uploadException) {
            echo $uploadException->getMessage() . PHP_EOL;
        }// .try upload contents
    }
}