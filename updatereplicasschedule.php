<?php
require 'vendor/autoload.php';

use Medoo\Medoo;

$db = new Medoo([
    'database_type' => 'mysql',
    'database_name' => 'automateit',
    'server' => 'database',
    'username' => 'user',
    'password' => 'jayapura',
    'charset' => 'utf8'
]);

$replicatinglists = $db->select('replicatinglists',
    [
        'id(replicatinglist_id)',
        'uploadat',
        'username',
        'account_id',
        'member_id'
    ]
);
foreach ($replicatinglists as $rl) {
    $replicas = $db->select('replicas',
        [
            'id(replica_id)',
            'schedule',
        ],
        [
            'AND' => [
                'active' => true,
                'account_id' => $rl['account_id'],
                'member_id' => $rl['member_id'],
            ],
            'ORDER' => ['takenat' => 'ASC']
        ]
    );

    if (count($replicas) > 0) {
        $t = 1;
        foreach ($replicas as $r) {
            $today = date_create_from_format('Y-m-d H:i:s', date('Y') . '-' . date('m') . '-' . date('d') . ' ' . $rl['uploadat']);
            $nextday = $today->modify('+' . $t . ' day');
            echo $nextday->format('Y-m-d H:i:s') . PHP_EOL;
            $db->update('replicas',
            ['schedule' => $nextday->format('Y-m-d H:i:s')],
            ['id' => $r['replica_id']]);
            $t++;
        }
    }
}