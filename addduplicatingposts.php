<?php
require 'vendor/autoload.php';

use InstagramAPI\Instagram;
use Medoo\Medoo;

require_once 'db.php';
/*$db = new Medoo([
    'database_type' => 'mysql',
    'database_name' => 'automateit',
    'server' => 'database',
    'username' => 'user',
    'password' => 'jayapura',
    'charset' => 'utf8',
]);*/

// Get batch to process
$batch = 0;
if (isset($argv[1])) {
    $batch = $argv[1];
}

while (true) {
    $ig = new Instagram(false, false);

    echo "Waiting for account(s) to process on " . date('d-m-Y H:i') . PHP_EOL;

    $accounts = $db->select('accounts',
        [
            '[<]proxies' => ['proxy_id' => 'id'],
        ],
        [
            'account' => [
                'accounts.id(account_id)',
                'accounts.user_id',
                'accounts.username',
                'accounts.password',
            ],
            'proxy' => [
                'proxies.id AS proxy_id',
                'proxies.name AS proxy_name',
            ],
        ],
        [
            'AND' => [
                'accounts.statusid' => 5,
                'accounts.active' => true,
            ],
        ]
    );
    foreach ($accounts as $a) {
        $idols = $db->select('replicatinglists',
            [
                '[<]accounts' => ['account_id' => 'id'],
            ],
            [
                'account' => [
                    'accounts.id(account_id)',
                    'accounts.username(account_username)',
                    'accounts.password(account_password)',
                ],
                'replicatinglist' => [
                    'replicatinglists.id(replicatinglist_id)',
                    'replicatinglists.username(replicatinglist_username)',
                    'replicatinglists.memberfixed',
                    'replicatinglists.contents(replicatinglist_contents)',
                    'replicatinglists.loads',
                    'replicatinglists.replicated',
                    'replicatinglists.replicatedat',
                    'replicatinglists.maxid',
                    'replicatinglists.uploadat',
                    'replicatinglists.note',
                ],
            ],
            [
                'AND' => [
                    'replicatinglists.account_id' => $a['account']['account_id'],
                    'replicatinglists.active' => true,
                ],
            ]);

        if (count($idols) > 0) {
            try {
                if ($a['proxy']['id'] > 1) {
                    $ig->setProxy($a['proxy']['name']);
                }

                $igLogin = $ig->login($a['account']['username'], $a['account']['password']);
                echo "Succeed to login to " . $a['account']['username'] . "\n";

                foreach ($idols as $idol) {
                    // first check if idol username is exists on members table
                    try {
                        $idolOnIg = $ig->people->getInfoByName($idol['replicatinglist']['replicatinglist_username'], 'following');
                        if ($idolOnIg->getStatus() == 'ok') {
                            echo "Getting info about " . $idolOnIg->getUser()->getUsername() . PHP_EOL;
                            $member_id = insertMember($db, $idolOnIg->getUser());
                            // update member_id on replicatinglists table
                            if (!$idol['replicatinglist']['memberfixed']) {
                                $db->update('replicatinglists',
                                    [
                                        'memberfixed' => true,
                                        'member_id' => $member_id,
                                        'contents' => $idolOnIg->getUser()->getMediaCount(),
                                        'note' => 'Terdapat akun',
                                    ],
                                    ['id' => $idol['replicatinglist']['replicatinglist_id']]
                                );
                            }
                            // get idol feed(s)
                            $maxId = null;
                            $postCounter = 0;
                            $postNotYetSaved = true;
                            do {
                                $response = $ig->timeline->getUserFeed($idolOnIg->getUser()->getPk(), $maxId);
                                $t = 1;
                                foreach ($response->getItems() as $item) {
                                    // Insert Location
                                    $location_id = 1;
                                    if ($item->hasLocation() && $item->getLocation() !== null) {
                                        $location_id = insertLocation($db, $item->getLocation());
                                    }

                                    // Check if posts is exists on db
                                    if (checkIsPostExists($db, $item)) {
                                        $postNotYetSaved = false;
                                    } else {
                                        // Insert Post
                                        $post_id = insertPost($db, $item, $location_id, $member_id);

                                        // Insert Wad
                                        if ($post_id > 0 && $item->getMediaType() == 1) { // Photo
                                            insertWad($db, $item->getImageVersions2()->getCandidates()[0], $post_id, $item->getMediaType());
                                            echo $postCounter . '. Getting photo' . PHP_EOL;
                                        } elseif ($post_id > 0 && $item->getMediaType() == 2) { // Video
                                            insertWad($db, $item->getVideoVersions()[0], $post_id, $item->getMediaType());
                                            echo $postCounter . '. Getting video' . PHP_EOL;
                                        } elseif ($post_id > 0 && $item->getMediaType() == 8) { // Carousel
                                            insertWad($db, $item->getCarouselMedia(), $post_id, $item->getMediaType());
                                            echo $postCounter . '. Getting carousel' . PHP_EOL;
                                        }

                                        // Insert Replica
                                        $lastPost = $db->select('replicas',
                                            ['schedule'],
                                            [
                                                'AND' => ['account_id' => $a['account']['account_id'], 'member_id' => $member_id],
                                                'ORDER' => ['schedule' => 'DESC'],
                                                'LIMIT' => 1,
                                            ]);
                                        $today = date_create_from_format('Y-m-d H:i:s', $lastPost[0]['schedule']);
                                        $nextday = $today->modify('+' . $t . ' day');
                                        $t++;

                                        insertReplica($db, $a['account']['account_id'], $member_id, $post_id, $item, $nextday);

                                        $postCounter++;
                                    }
                                }
                                $maxId = $response->getNextMaxId();

                                // Update replicatinglists
                                $loads = $db->select('replicatinglists',
                                    ['loads', 'contents'],
                                    ['id' => $idol['replicatinglist']['replicatinglist_id'], 'LIMIT' => 1]
                                );
                                $newLoads = intval($loads[0]['loads']) + $response->getNumResults();
                                $replicated = false;
                                if ($newLoads >= intval($loads[0]['contents'])) {
                                    $replicated = true;
                                }

                                $db->update('replicatinglists',
                                    [
                                        'maxid' => $maxId,
                                        'loads' => $newLoads,
                                        'replicated' => $replicated,
                                        'replicatedat' => date('Y-m-d H:i:s'),
                                    ],
                                    ['id' => $idol['replicatinglist']['replicatinglist_id']]
                                );

                                sleep(rand(12, 17));
                            } while ($postNotYetSaved && $maxId !== null); // .loop for all idol feed(s)
                        } // .if status ok
                    } catch (Exception $ex) {
                        if (strpos($ex->getMessage(), 'User not found.') !== false) {
                            echo $idol['replicatinglist']['replicatinglist_username'] . "\033[31m Not Found!\033[0m" . PHP_EOL;
                            $db->update('replicatinglists',
                                [
                                    'memberfixed' => false,
                                    'note' => 'Tidak ada user Ig dengan nama ' . $idol['replicatinglist']['replicatinglist_username'],
                                ],
                                ['id' => $idol['replicatinglist']['replicatinglist_id']]
                            );
                        } else {
                            echo $ex->getMessage() . PHP_EOL;
                        }
                    } // .try get idol info from ig
                } // .foreach idol (account that'll duplicating it posts)
            } catch (Exception $e) {
                echo $e . PHP_EOL;
            } // .try login ig
        }
    } // .foreach accounts
    sleep(1800);
} // .while true

function insertReplica($db = null, $account_id = 1, $member_id = 1, $post_id = 1, $datum = null, $schedule = null)
{
    $check = $db->select('replicas',
        ['id'],
        ['account_id' => $account_id, 'member_id' => $member_id, 'post_id' => $post_id, 'active' => true]
    );

    if (count($check) > 0) {
        return $check[0]['id'];
    } else {
        ($datum->hasCaption() && $datum->getCaption() !== null) ? $caption = $datum->getCaption()->getText() : $caption = '';

        $db->insert('replicas',
            [
                'account_id' => $account_id,
                'member_id' => $member_id,
                'post_id' => $post_id,
                'typeid' => $datum->getMediaType(),
                'caption' => $caption,
                'takenat' => $datum->getTakenAt(),
                'schedule' => $schedule->format('Y-m-d H:i:s'),
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ]
        );
        return $db->id();
    } //if not exists on table
} // .function insertReplica

function insertMember($db = null, $datum = null)
{
    $check = $db->select('members',
        ['id'],
        ['pk' => $datum->getPk(), 'active' => true]
    );

    ($datum->hasProfilePicUrl()) ? $profpicurl = $datum->getProfilePicUrl() : $profpicurl = '';

    if (count($check) > 0) {
        $db->update('members',
            [
                'fullname' => $datum->getFullName(),
                'description' => $datum->getBiography(),
                'profpicurl' => $profpicurl,
                'followers' => $datum->getFollowerCount(),
                'followings' => $datum->getFollowingCount(),
                'contents' => $datum->getMediaCount(),
                'profpicurlfixed' => true,
                'modified' => date('Y-m-d H:i:s'),

            ],
            [
                'pk' => $datum->getPk(),
            ]
        );
        return $check[0]['id'];
    } else { // save member to db
        $db->insert('members',
            [
                'pk' => $datum->getPk(),
                'username' => $datum->getUsername(),
                'fullname' => $datum->getFullName(),
                'description' => $datum->getBiography(),
                'profpicurl' => $profpicurl,
                'followers' => $datum->getFollowerCount(),
                'followings' => $datum->getFollowingCount(),
                'contents' => $datum->getMediaCount(),
                'profpicurlfixed' => true,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ]
        );
        return $db->id();
    }
} // .function insertMember

function insertWad($db = null, $datum = null, $post_id = 1, $typeid = 1)
{
    $newData = [
        'post_id' => $post_id,
        'typeid' => $typeid,
        'sequence' => 0,
    ];

    if ($typeid == 1 || $typeid == 2) { // Single photo or video
        $check = $db->select('wads',
            ['id'],
            ['post_id' => $post_id, 'url' => $datum->getUrl(), 'active' => true]
        );

        if (count($check) < 1) {
            $newData['url'] = $datum->getUrl();
            $newData['width'] = $datum->getWidth();
            $newData['height'] = $datum->getHeight();
            $db->insert('wads', $newData);
            return $db->id();
        } else {
            return $check[0]['id'];
        }
    } elseif ($typeid == 8) { // Carousel
        $sequence = 0;
        foreach ($datum as $d) {
            $newData['url'] = '';
            $newData['width'] = 0;
            $newData['height'] = 0;
            $newData['typeid'] = $d->getMediaType();
            if ($d->getMediaType() == 1) { // Photo
                $reap = $d->getImageVersions2()->getCandidates()[0];
            } elseif ($d->getMediaType() == 2) { // Video
                $reap = $d->getVideoVersions()[0];
            }
            $check = $db->select('wads',
                ['id'],
                ['post_id' => $post_id, 'url' => $reap->getUrl(), 'active' => true]
            );

            if (count($check) < 1) {
                $newData['url'] = $reap->getUrl();
                $newData['width'] = $reap->getWidth();
                $newData['height'] = $reap->getHeight();
                $newData['sequence'] = $sequence;
                $sequence++;
                $db->insert('wads', $newData);
            }
        }
    }
} // .insertWad

function checkIsPostExists($db = null, $datum = null)
{
    $check = $db->select('posts',
        ['id'],
        ['pk' => $datum->getPk(), 'active' => true]
    );

    if (count($check) > 0) {
        return true;
    } else {
        return false;
    }
}

function insertPost($db = null, $datum = null, $location_id = 1, $member_id = 1)
{
    $check = $db->select('posts',
        ['id'],
        ['pk' => $datum->getPk(), 'active' => true]
    );

    if (count($check) > 0) {
        return $check[0]['id'];
    } else { // save location to db
        ($datum->hasCaption() && $datum->getCaption() !== null) ? $caption = $datum->getCaption()->getText() : $caption = '';
        //echo "New post with caption $caption";

        $db->insert('posts',
            [
                'pk' => $datum->getPk(),
                'sourceid' => $datum->getId(),
                'location_id' => $location_id,
                'member_id' => $member_id,
                'typeid' => $datum->getMediaType(),
                'caption' => $caption,
                'likes' => $datum->getLikeCount(),
                'comments' => $datum->getCommentCount(),
                'takenat' => $datum->getTakenAt(),
            ]
        );
        return $db->id();
    }
} // .function insertPost

function insertLocation($db = null, $location = null)
{
    $check = $db->select('locations',
        ['id'],
        ['pk' => $location->getPk(), 'active' => true]
    );

    if (count($check) > 0) {
        return $check[0]['id'];
    } else { // save location to db
        //echo "New location named " . $location->getName() . "\n";
        ($location->hasFacebookPlacesId()) ? $fbplacesid = $location->getFacebookPlacesId() : $fbplacesid = 0;

        $db->insert('locations',
            [
                'pk' => $location->getPk(),
                'lat' => $location->getLat(),
                'lng' => $location->getLng(),
                'shortname' => $location->getShortName(),
                'name' => $location->getName(),
                'address' => $location->getAddress(),
                'fbplacesid' => $fbplacesid,

            ]
        );
        return $db->id();
    }
} // .function insertLocation
