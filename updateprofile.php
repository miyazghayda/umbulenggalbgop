<?php
require 'vendor/autoload.php';

use Medoo\Medoo;
use InstagramScraper\Instagram;

$db = new Medoo([
    'database_type' => 'mysql',
    'database_name' => 'automateit',
    'server' => 'database',
    'username' => 'user',
    'password' => 'jayapura',
    'charset' => 'utf8'
]);

echo "Waiting for account(s) to process on " . date('d-m-Y H:i') . "\n";

$accounts = $db->select('accounts',
    [
        'account' => [
            'accounts.id(account_id)',
            'accounts.pk'
        ]
    ],
    [
        'AND' => [
            'accounts.statusid' => 5,
            'accounts.active' => true,
        ]
    ]
);

$ig = new Instagram();
foreach ($accounts as $a) {
    $account = $ig->getAccountById($a['account']['pk']);
    echo "updated account " . $account->getFullName() . "\n";
    $db->update('accounts',
        [
            'profpicurl' => $account->getProfilePicUrl(),
            'fullname' => $account->getFullName(),
            'description' => $account->getBiography(),
            'followers' => $account->getFollowedByCount(),
            'followings' => $account->getFollowsCount(),
            'contents' => $account->getMediaCount(),
            'profpicurlfixed' => true
        ],
        ['id' => $a['account']['account_id']]);
    sleep(5);
    //echo $account->isPrivate();
}
