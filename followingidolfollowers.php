<?php
require 'vendor/autoload.php';

use InstagramAPI\Exception;
use InstagramAPI\Instagram;
use Medoo\Medoo;

require_once 'db.php';
/*$db = new Medoo([
    'database_type' => 'mysql',
    'database_name' => 'automateit',
    'server' => 'database',
    'username' => 'user',
    'password' => 'jayapura',
    'charset' => 'utf8',
]);*/

// Get batch to process
$batch = 0;
if (isset($argv[1])) {
    $batch = $argv[1];
}

while (true) {
    echo "File \033[34m" . basename(__FILE__, '.php') . "\033[0m Waiting for account(s) to process on " . date('d-m-Y H:i') . PHP_EOL;

    $accountsOnFollowinglist = $db->select('accountlists',
        ['account_id'],
        [
            'AND' => [
                'typeid' => 1,
                'allfollowersaved' => false,
                'active' => true,
            ],
            'GROUP' => ['account_id'],
        ]);

    // Remove redundant account_id
    $accountIds = [];
    foreach ($accountsOnFollowinglist as $a) {
        if (!in_array($a['account_id'], $accountIds)) {
            array_push($accountIds, $a['account_id']);
        }

    }
    // Check if account_id is paid account
    $paidAccountIds = [];
    foreach ($accountIds as $a) {
        $check = $db->count('accounts',
            [
                'AND' => [
                    'id' => $a,
                    'statusid' => 5,
                    'active' => true,
                ],
            ]);
        if ($check > 0) {
            array_push($paidAccountIds, $a);
        }
    }

    foreach ($paidAccountIds as $account_id) {
        $accountlists = $db->select('accountlists',
            [
                '[<]accounts' => ['account_id' => 'id'],
                '[<]members' => ['member_id' => 'id'],
                '[<]filters' => ['filter_id' => 'id'],
                '[>]preferences' => ['account_id' => 'account_id'],
                '[<]proxies' => ['accounts.proxy_id' => 'id'],
                '[<]vassals' => ['vassal_id' => 'id'],
                '[<]celebrities' => ['celebrity_id' => 'id'],
            ],
            [
                'accountlist' => [
                    'accountlists.id(accountlist_id)',
                    'accountlists.typeid(accountlist_typeid)',
                    'accountlists.allfollowersaved',
                    'accountlists.vassal_id',
                ],
                'account' => [
                    'accounts.id(account_id)',
                    'accounts.proxy_id',
                    'accounts.user_id',
                    'accounts.username',
                    'accounts.password',
                ],
                'celebrity' => [
                    'celebrities.id(celebrity_id)',
                    'celebrities.followers(celebrity_followers)',
                    'celebrities.followersaved(celebrity_followersaved)',
                    'celebrities.allfollowersaved(celebrity_allfollowersaved)',
                ],
                'vassal' => [
                    'vassals.id(vassal_id)',
                ],
                'member' => [
                    'members.id(member_id)',
                    'members.pk(member_pk)',
                    'members.username(member_username)',
                    'members.fullname(member_fullname)',
                    'members.followers(member_followers)',
                ],
                'filter' => [
                    'filters.id(filter_id)',
                    'filters.typeid(filter_typeid)',
                    'filters.hashtagblacklist',
                    'filters.fullnameblacklist',
                    'filters.biographyblacklist',
                    'filters.nmonths',
                    'filters.accounttypeid',
                ],
                'preference' => [
                    'preferences.id(preference_id)',
                    'preferences.maxfollowperday',
                    'preferences.followtoday',
                    'preferences.idolfollowertofollowtoday',
                    'preferences.getidolfollowertofollowtoday',
                    'preferences.followbyhashtag',
                    'preferences.followbylocation',
                    'preferences.followidolfollower',
                ],
                'proxy' => [
                    'proxies.id AS proxy_id',
                    'proxies.name AS proxy_name',
                ],
            ],
            [
                'AND' => [
                    'accountlists.account_id' => $account_id,
                    'accountlists.typeid' => 1,
                    'accountlists.allfollowersaved' => false,
                    'accountlists.active' => true,
                    'preferences.followbatch' => $batch,
                    'preferences.followidolfollower' => true,
                    'preferences.maxfollowperday[>]' => 0,
                    'preferences.getidolfollowertofollowtoday' => true,
                ],
                'LIMIT' => 1,
                'ORDER' => [
                    'accountlists.id' => 'ASC',
                ],
            ]
        );
        //print_r($accountlists);

        foreach ($accountlists as $account) {
            $ig = new Instagram(false, false);
            try {
                if ($account['account']['proxy_id'] > 1) {
                    $ig->setProxy($account['proxy']['proxy_name']);
                }

                echo 'Login with username ' . $account['account']['username'] . PHP_EOL;
                // Login akun IG
                $ig->login($account['account']['username'], $account['account']['password']);
            } catch (\Exception $e) {
                echo $e . PHP_EOL;
            } // .try login

            $maxFollowTodayDivider = 1;
            if ($account['preference']['followbyhashtag']) {
                $maxFollowTodayDivider++;
            }

            if ($account['preference']['followbylocation']) {
                $maxFollowTodayDivider++;
            }

            $maxFollowPerDay = $account['preference']['maxfollowperday'];

            $maxFollowPerDayPerMode = floor($maxFollowPerDay / $maxFollowTodayDivider);
            $followToday = $account['preference']['followtoday'];

            // different per mode
            $followTodayIdolFollower = $account['preference']['idolfollowertofollowtoday'];
            $toFollowNow = $maxFollowPerDayPerMode - $followTodayIdolFollower;

            // if filter is on, double the vassals to gather
            if ($account['filter']['filter_id'] > 1) {
                $toFollowNow = $toFollowNow * 2;
            }
            $fellowFollowedCount = 0;
            // Comment below line on production
            //$toFollowNow = 1;

            // Get idol follower to follow now
            $vassals = $db->select('vassals',
                [
                    '[<]members' => ['member_id' => 'id'],
                    '[<]fellows' => ['fellow_id' => 'id'],
                ],
                [
                    'vassal' => [
                        'vassals.id(vassal_id)',
                    ],
                    'member' => [
                        'members.id(member_id)',
                        'members.pk(member_pk)',
                        'members.username(member_username)',
                    ],
                    'fellow' => [
                        'fellows.id(fellow_id)',
                        'fellows.pk(fellow_pk)',
                        'fellows.username(fellow_username)',
                        'fellows.fullname(fellow_fullname)',
                        'fellows.description(fellow_description)',
                        'fellows.closed(fellow_closed)',
                    ],
                ],
                [
                    'AND' => [
                        'vassals.member_id' => $account['member']['member_id'],
                        'vassals.active' => true,
                        'vassals.id[>]' => $account['accountlist']['vassal_id'],
                    ],
                    'ORDER' => ['vassals.id' => 'ASC'],
                    'LIMIT' => $toFollowNow,
                ]
            );

            foreach ($vassals as $v) {
                // if no filter, follow vassal directly
                $followThisFellow = true;
                $reasonToNotFollow = [];

                // if filter used, filtering first before decide to follow
                if ($account['filter']['filter_id'] > 1) {
                    if (!empty($account['filter']['fullnameblacklist'])) {
                        $strips = explode(',', $account['filter']['fullnameblacklist']);
                        foreach ($strips as $s) {
                            $s = strtolower($s);
                            $fullname = strtolower($v['fellow']['fellow_fullname']);
                            if (strpos($fullname, $s) !== false) {
                                $followThisFellow = false;
                                array_push($reasonToNotFollow, 'fullname filled with ' . $s);
                            }
                        }
                    }

                    // don't follow fellow with biography/description is blacklisting
                    if (!empty($account['filter']['biographyblacklist'])) {
                        $strips = explode(',', $account['filter']['biographyblacklist']);
                        foreach ($strips as $s) {
                            $s = strtolower($s);
                            $fullname = strtolower($v['fellow']['fellow_description']);
                            if (strpos($fullname, $s) !== false) {
                                $followThisFellow = false;
                                array_push($reasonToNotFollow, 'biography filled with ' . $s);
                            }
                        }
                    }

                    // only follow public fellow
                    if ($account['filter']['accounttypeid'] == 2) {
                        if ($v['fellow']['fellow_closed'] == true) {
                            $followThisFellow = false;
                            array_push($reasonToNotFollow, 'fellow is private');
                        }
                    }

                    // only follow private fellow
                    if ($account['filter']['accounttypeid'] == 3) {
                        if ($v['fellow']['fellow_closed'] == false) {
                            $followThisFellow = false;
                            array_push($reasonToNotFollow, 'fellow is public');
                        }
                    }

                    if ($account['filter']['nmonths'] > 0) {
                        echo 'Gather ' . $v['fellow']['fellow_username'] . ' posts' . PHP_EOL;
                        $lastPostTakenAt = getFellowLastPost($ig, $db, $v['fellow']);

                        $nMonthsAgo = strtotime(date('Y-m-d', strtotime(date('Y-m-d', strtotime(date('Y-m-d'))) . '-' . $account['filter']['nmonths'] . ' month')));
                        if ($lastPostTakenAt < $nMonthsAgo) {
                            $followThisFellow = false;
                            array_push($reasonToNotFollow, 'last post is ' . date('d-m-Y', $lastPostTakenAt) . ', older than ' . $account['filter']['nmonths'] . ' months ago.');
                        }
                    }
                } // .if filter is on

                if ($followThisFellow && $fellowFollowedCount < $toFollowNow) {
                    try {
                        echo $account['account']['username'] . ' will following ' . $v['fellow']['fellow_username'] .
                            ' (follower of ' . $v['member']['member_username'] . ')' . PHP_EOL;

                        $ig->people->follow($v['fellow']['fellow_pk']);
                        insertFollowinglist($db, $account['account']['account_id'], $v['vassal']['vassal_id'], $v['member']['member_id'], $v['fellow']['fellow_id']);
                        $fellowFollowedCount++;
                    } catch (\Exception $e) {
                        echo $e->getMessage() . PHP_EOL;
                        // if error, ie fellow account is deleted
                        $db->update('vassals',
                            ['active' => false],
                            ['id' => $v['vassal']['vassal_id']]);
                    }
                    sleep(rand(28, 38));
                } else {
                        echo $account['account']['username'] . " will\033[31m NOT\033[0m following " . $v['fellow']['fellow_username'] .
                            " (follower of " . $v['member']['member_username'] . ") because " . implode(',', $reasonToNotFollow) . PHP_EOL;
                }// .if follow this fellow
            } // .foreach vassals

            // update accountlist
            $db->update('accountlists',
                ['vassal_id' => end($vassals)['vassal']['vassal_id']],
                ['id' => $account['accountlist']['accountlist_id']]);

            // update preferences
            $db->update('preferences',
                [
                    'followtoday' => $account['preference']['followtoday'] + $fellowFollowedCount,
                    'idolfollowertofollowtoday' => $fellowFollowedCount,
                    'getidolfollowertofollowtoday' => false
                ],[
                    'id' => $account['preference']['preference_id']
                ]);

            /*$db->update('accountlists',
        ['vassal_id' => $vassals[count($vassals) - 1]['vassal']['vassal_id']],
        ['id' => $account['accountlist']['accountlist_id']]
        );*/
        } // .foreach accountlist
    } // .foreach paid account

    sleep(1800);

} // .while true

function getFellowLastPost($ig = null, $db = null, $fellow = null)
{
    $lastPostTakenAt = 0;
    try {
        $response = $ig->timeline->getUserFeed($fellow['fellow_pk'], null);
        //$response->printPropertyDescriptions();

        if ($response->getStatus() == 'ok' && $response->getNumResults() > 0) {
            // update array
            //array_push($fellowIdsWithPost, $f);
            $lastPostTakenAt = $response->getItems()[0]->getTakenAt();

            foreach ($response->getItems() as $item) {
                // Insert Location
                $location_id = 1;
                if ($item->hasLocation() && $item->getLocation() !== null) {
                    $location_id = insertLocation($db, $item->getLocation());
                }

                // Insert Post
                $post_id = insertPost($db, $item, $location_id, $fellow['fellow_id']);

                // Insert Wad
                if ($post_id > 0 && $item->getMediaType() == 1) { // Photo
                    insertWad($db, $item->getImageVersions2()->getCandidates()[0], $post_id, $item->getMediaType());
                } elseif ($post_id > 0 && $item->getMediaType() == 2) { // Video
                    insertWad($db, $item->getVideoVersions()[0], $post_id, $item->getMediaType());
                } elseif ($post_id > 0 && $item->getMediaType() == 8) { // Carousel
                    insertWad($db, $item->getCarouselMedia(), $post_id, $item->getMediaType());
                }
            }
        }
    } catch (\Exception $e) {
        echo $e->getMessage() . PHP_EOL;
    }
    return $lastPostTakenAt;
}

function insertWad($db = null, $datum = null, $post_id = 1, $typeid = 1)
{
    $newData = [
        'post_id' => $post_id,
        'typeid' => $typeid,
        'sequence' => 0,
    ];

    if ($typeid == 1 || $typeid == 2) { // Single photo or video
        $newData['url'] = $datum->getUrl();
        $newData['width'] = $datum->getWidth();
        $newData['height'] = $datum->getHeight();
        $db->insert('wads', $newData);
        return $db->id();
    } elseif ($typeid == 8) { // Carousel
        $sequence = 0;
        foreach ($datum as $d) {
            $newData['url'] = '';
            $newData['width'] = 0;
            $newData['height'] = 0;
            $newData['typeid'] = $d->getMediaType();
            if ($d->getMediaType() == 1) { // Photo
                $reap = $d->getImageVersions2()->getCandidates()[0];
            } elseif ($d->getMediaType() == 2) { // Video
                $reap = $d->getVideoVersions()[0];
            }
            $newData['url'] = $reap->getUrl();
            $newData['width'] = $reap->getWidth();
            $newData['height'] = $reap->getHeight();
            $newData['sequence'] = $sequence;
            $sequence++;
            $db->insert('wads', $newData);
        }
    }
} // .insertWad

function insertPost($db = null, $datum = null, $location_id = 1, $member_id = 1)
{
    $check = $db->select('posts',
        ['id'],
        ['pk' => $datum->getPk(), 'active' => true]
    );

    if (count($check) > 0) {
        return $check[0]['id'];
    } else { // save location to db
        ($datum->hasCaption() && $datum->getCaption() !== null) ? $caption = $datum->getCaption()->getText() : $caption = '';
        //echo "New post with caption $caption";

        $db->insert('posts',
            [
                'pk' => $datum->getPk(),
                'sourceid' => $datum->getId(),
                'location_id' => $location_id,
                'member_id' => $member_id,
                'typeid' => $datum->getMediaType(),
                'caption' => $caption,
                'likes' => $datum->getLikeCount(),
                'comments' => $datum->getCommentCount(),
                'takenat' => $datum->getTakenAt(),
            ]
        );
        return $db->id();
    }
} // .function insertPost

function insertLocation($db = null, $location = null)
{
    $check = $db->select('locations',
        ['id'],
        ['pk' => $location->getPk(), 'active' => true]
    );

    if (count($check) > 0) {
        return $check[0]['id'];
    } else { // save location to db
        //echo "New location named " . $location->getName() . "\n";
        ($location->hasFacebookPlacesId()) ? $fbplacesid = $location->getFacebookPlacesId() : $fbplacesid = 0;

        $db->insert('locations',
            [
                'pk' => $location->getPk(),
                'lat' => $location->getLat(),
                'lng' => $location->getLng(),
                'shortname' => $location->getShortName(),
                'name' => $location->getName(),
                'address' => $location->getAddress(),
                'fbplacesid' => $fbplacesid,

            ]
        );
        return $db->id();
    }
} // .function insertLocation

function insertFollowinglist($db = null, $account_id = 1, $vassal_id = 1, $member_id = 1, $fellow_id = 1)
{
    $check = $db->select('followinglists',
        ['id'],
        ['fellow_id' => $fellow_id, 'vassal_id' => $vassal_id, 'member_id' => $member_id, 'account_id' => $account_id, 'active' => true]
    );

    if (count($check) > 0) {
        return $check[0]['id'];
    } else { // save location to db
        $db->insert('followinglists',
            [
                'vassal_id' => $vassal_id,
                'member_id' => $member_id,
                'fellow_id' => $fellow_id,
                'typeid' => 1,
                'account_id' => $account_id,
                'note' => '',
                'followed' => true,
                'unfollowed' => false,
                'followedat' => date('Y-m-d H:i:s'),
                'who' => false,
            ]
        );
        return $db->id();
    }
} // .insert Followinglist
