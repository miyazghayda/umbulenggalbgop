<?php
require 'vendor/autoload.php';

use Medoo\Medoo;
use InstagramAPI\Instagram;
use InstagramAPI\Constants;
use InstagramAPI\Signatures;
use InstagramAPI\Exception;

$db = new Medoo([
    'database_type' => 'mysql',
    'database_name' => 'automateit',
    'server' => 'localhost',
    'username' => 'user',
    'password' => 'jayapura',
    'charset' => 'utf8'
]);

// Get batch to process
$batch = 0;
if (isset($argv[1])) $batch = $argv[1];

while (true) {
    $ig = new Instagram(false, false);

    echo "File \033[34m" . basename(__FILE__, '.php') . "\033[0m Waiting for account(s) to process on " . date('d-m-Y H:i') . PHP_EOL;

    //echo 'File ' . basename(__FILE__, '.php') . " Waiting for account(s) to process on " . date('d-m-Y H:i') . "\n";
    $accountlists = $db->select('accountlists',
        [
            '[<]accounts' => ['account_id' => 'id'],
            '[<]members' => ['member_id' => 'id'],
            '[<]filters' => ['filter_id' => 'id'],
            '[>]preferences' => ['account_id' => 'account_id'],
            '[<]proxies' => ['accounts.proxy_id' => 'id']
        ],
        [
            'accountlist' => [
                'accountlists.id(accountlist_id)',
                'accountlists.typeid(accountlist_typeid)',
                'accountlists.proffixed',
                'accountlists.allfollowersaved',
                'accountlists.nextmaxid'
            ],
            'account' => [
                'accounts.id(account_id)',
                'accounts.proxy_id',
                'accounts.user_id',
                'accounts.username',
                'accounts.password'
            ],
            'member' => [
                'members.id(member_id)',
                'members.pk(member_pk)',
                'members.username(member_username)',
                'members.fullname(member_fullname)',
                'members.followers(member_followers)'
            ],
            'filter' => [
                'filters.id(filter_id)',
                'filters.typeid(filter_typeid)',
                'filters.hashtagblacklist',
                'filters.fullnameblacklist',
                'filters.biographyblacklist',
                'filters.nmonths',
                'filters.accounttypeid'
            ],
            'preference' => [
                'preferences.maxfollowperday',
                'preferences.followtoday',
                'preferences.idolfollowertofollowtoday',
                'preferences.getidolfollowertofollowtoday',
            ],
            'proxy' => [
                'proxies.id AS proxy_id',
                'proxies.name AS proxy_name'
            ]
        ],
        [
            'AND' => [
                'accountlists.typeid' => 1,
                'accountlists.allfollowersaved' => false,
                'accountlists.active' => true,
                'preferences.followbatch' => $batch,
                'preferences.followidolfollower' => true,
                'preferences.maxfollowperday[>]' => 0,
                'preferences.getidolfollowertofollowtoday' => true
            ]
        ]
    );
   
    foreach ($accountlists as $account) {
        try {
            if ($account['account']['proxy_id'] > 1) $ig->setProxy($account['proxy']['proxy_name']);
            echo 'Login with username ' . $account['account']['username'] . PHP_EOL;
            // Login akun IG
            $ig->login($account['account']['username'], $account['account']['password']);
        } catch (\Exception $e) {
            echo $e . PHP_EOL;
        }// .try login

        $fellowIds = [];
        try {
            $rankToken = \InstagramAPI\Signatures::generateUUID();
            $maxId = $account['accountlist']['nextmaxid'];
            empty($maxId) ? $maxId = null : $maxId = $account['accountlist']['nextmaxid'];

            echo 'Now trying to gather ' . $account['member']['member_username'] . ' followers' . PHP_EOL;
            $i = 0;
            do {
                $response = $ig->people->getFollowers($account['member']['member_pk'], $rankToken, null, $maxId);
                if ($response->getStatus() === 'ok') {
                    foreach ($response->getUsers() as $u) {
                        // Insert Member
                        $fellow_id = insertMember($db, $u);
                        array_push($fellowIds, $fellow_id);

                        // Insert Vassal
                        $vassal_id = insertVassal($db, $account['member']['member_id'], $fellow_id);

                        /*$followinglist_id = insertFollowinglist($db, $account['account']['account_id'],
                            $vassal_id, $account['member']['member_id'], $fellow_id);*/
                       
                        echo $i . '. saving ' . $u->getUsername() . PHP_EOL;
                        $i++;
                    }
                }

                $maxId = $response->getNextMaxId();
                $db->update('accountlists',
                    ['nextmaxid' => $maxId],
                    ['id' => $account['accountlist']['accountlist_id']]);
                
                // If end of followers
                if ($maxId === null) {
                    $db->update('accountlists',
                        ['allfollowersaved' => true],
                        ['id' => $account['accountlist']['accountlist_id']]);
                }

                // comment this on production
                // $maxId = null;
                sleep(rand(7, 12));
            } while ($maxId !== null);
        } catch (\Exception $e) {
            echo $e->getMessage() . PHP_EOL;
        }// .try gather followers

        // get fellow last post
        $i = 0;
        if (count($fellowIds) > 0) {
            foreach ($fellowIds as $f) {
                $v = $db->select('members',
                ['id', 'pk', 'username', 'fullname', 'description', 'closed'],
                ['id' => $f],
                ['LIMIT' => 1]);
                $v = $v[0];
                echo $i . ' Get post(s) of ' . $v['username'] . PHP_EOL;
                $i++;

                $lastPostTakenAt = 0;
                try {
                    $response = $ig->timeline->getUserFeed($v['pk'], null);
                    //$response->printPropertyDescriptions();

                    if ($response->getStatus() == 'ok' && $response->getNumResults() > 0) {
                        // update array
                        //array_push($fellowIdsWithPost, $f);
                        $lastPostTakenAt = $response->getItems()[0]->getTakenAt();

                        foreach ($response->getItems() as $item) {
                            // Insert Location
                            $location_id = 1;
                            if ($item->hasLocation() && $item->getLocation() !== null) {
                                $location_id = insertLocation($db, $item->getLocation());
                            }

                            // Insert Post
                            $post_id = insertPost($db, $item, $location_id, $f);

                            // Insert Wad
                            if ($post_id > 0 && $item->getMediaType() == 1) {// Photo
                                insertWad($db, $item->getImageVersions2()->getCandidates()[0], $post_id, $item->getMediaType());
                            } elseif ($post_id > 0 && $item->getMediaType() == 2) {// Video
                                insertWad($db, $item->getVideoVersions()[0], $post_id, $item->getMediaType());
                            } elseif ($post_id > 0 && $item->getMediaType() == 8) {// Carousel
                                insertWad($db, $item->getCarouselMedia(), $post_id, $item->getMediaType());
                            }
                        }
                    }
                } catch(\Exception $e) {
                    echo $e->getMessage() . PHP_EOL;
                }
                // filtering
                $followThisFellow = true;
                $reasonToNotFollow = [];
                if (!empty($account['filter']['fullnameblacklist'])) {
                    $strips = explode(',', $account['filter']['fullnameblacklist']);
                    foreach ($strips as $s) {
                        $s = strtolower($s);
                        $fullname = strtolower($v['fullname']);
                        if (strpos($fullname, $s) !== false) {
                            $followThisFellow = false;
                            array_push($reasonToNotFollow, 'fullname filled with ' . $s);
                        }
                    }
                }
                
                // don't follow fellow with biography/description is blacklisting
                if (!empty($account['filter']['biographyblacklist'])) {
                    $strips = explode(',', $account['filter']['biographyblacklist']);
                    foreach ($strips as $s) {
                        $s = strtolower($s);
                        $fullname = strtolower($v['description']);
                        if (strpos($fullname, $s) !== false) {
                            $followThisFellow = false;
                            array_push($reasonToNotFollow, 'biography filled with ' . $s);
                        }
                    }
                }

                if ($account['filter']['nmonths'] > 0) {
                    $nMonthsAgo = strtotime(date('Y-m-d', strtotime(date('Y-m-d', strtotime(date('Y-m-d'))) . '-' . $account['filter']['nmonths'] . ' month')));
                    if ($lastPostTakenAt < $nMonthsAgo) {
                        $followThisFellow = false;
                        array_push($reasonToNotFollow, 'last post is ' . date('d-m-Y', $lastPostTakenAt) . ', older than ' . $account['filter']['nmonths'] . ' months ago.');
                    }
                }

                // only follow public fellow
                if ($account['filter']['accounttypeid'] == 2) {
                    if ($v['closed'] == true) {
                        $followThisFellow = false;
                        array_push($reasonToNotFollow, 'fellow is private');
                    }
                }

                // only follow private fellow
                if ($account['filter']['accounttypeid'] == 3) {
                    if ($v['closed'] == false) {
                        $followThisFellow = false;
                        array_push($reasonToNotFollow, 'fellow is public');
                    }
                }

                if ($followThisFellow) {
                    echo $account['account']['username'] . ' will following ' . $v['username'] . PHP_EOL;
                    $vassal = $db->select('vassals',
                        ['id'],
                        ['member_id' => $account['member']['member_id'], 'fellow_id' => $v['id']],
                        ['LIMIT' => 1]);

                    $vassal_id = $vassal[0]['id'];
                    $followinglist_id = insertFollowinglist($db, $account['account']['account_id'],
                            $vassal_id, $account['member']['member_id'], $v['id']);
                } else {
                    echo $account['account']['username'] . ' won\'t follow ' . $v['username'] . ' because ' . implode(' ', $reasonToNotFollow) . PHP_EOL;
                }
                sleep(rand(7, 12));
            }// .foreach fellows
        }// .if fellows > 0

        /*echo 'Now trying to filter idol followers to follow' . PHP_EOL;
        if (count($fellowIds) > 0) {
            foreach ($fellowIds as $f) {
                $v = $db->select('members',
                    [

                    ],
                    ['id', 'pk', 'username', 'fullname'],
                    ['id' => $f],
                    ['LIMIT' => 1]);

            }// .foreach fellow
        }// .if fellow > 0*/
    }// .foreach accountlists

    sleep(1800);

}// .while true

function filtering($fellow, $filter) {

}

function insertFollowinglist($db = null, $account_id = 1, $vassal_id = 1, $member_id = 1, $fellow_id = 1) {
    $check = $db->select('followinglists',
        ['id'],
        ['fellow_id' => $fellow_id, 'vassal_id' => $vassal_id, 'member_id' => $member_id, 'account_id' => $account_id, 'active' => true]
    );

    if (count($check) > 0) {
        return $check[0]['id'];
    } else {// save location to db
        $db->insert('followinglists', 
            [
                'vassal_id' => $vassal_id,
                'member_id' => $member_id,
                'fellow_id' => $fellow_id,
                'typeid' => 1,
                'account_id' => $account_id,
                'note' => ''
            ]
        );
        return $db->id();
    }
}// .insert Followinglist

function insertVassal($db = null, $member_id = 1, $fellow_id = 1) {
    $check = $db->select('vassals',
        ['id'],
        ['member_id' => $member_id, 'fellow_id' => $fellow_id, 'active' => true]
    );

    if (count($check) > 0) {
        return $check[0]['id'];
    } else {// save vassal (idol follower) to db
        $db->insert('vassals',[
            'member_id' => $member_id,
            'fellow_id' => $fellow_id,
        ]);
        return $db->id();
    }
}// .function insertVassal

function insertMember($db = null, $datum = null) {
    $check = $db->select('members',
        ['id'],
        ['pk' => $datum->getPk(), 'active' => true]
    );

    if (count($check) > 0) {
        $db->update('members',
            ['closed' => $datum->getIsPrivate(), 'modified' => date('Y-m-d H:i:s')],
            ['id' => $check[0]['id']]);
        return $check[0]['id'];
    } else {// save member to db
        //echo "New member named " . $datum->getUsername() . "\n";
        ($datum->hasProfilePicUrl()) ? $profpicurl = $datum->getProfilePicUrl() : $profpicurl = '';

        $db->insert('members', 
            [
                'pk' => $datum->getPk(),
                'username' => $datum->getUsername(),
                'fullname' => $datum->getFullName(),
                'profpicurl' => $profpicurl,
                'closed' => $datum->getIsPrivate(),
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ]
        );
        return $db->id();
    }
}// .function insertMember

function insertWad($db = null, $datum = null, $post_id = 1, $typeid = 1) {
    $newData = [
        'post_id' => $post_id,
        'typeid' => $typeid,
        'sequence' => 0
    ];

    if ($typeid == 1 || $typeid == 2) {// Single photo or video
        $newData['url'] = $datum->getUrl();
        $newData['width'] = $datum->getWidth();
        $newData['height'] = $datum->getHeight();
        $db->insert('wads', $newData);
        return $db->id();
    } elseif ($typeid == 8) {// Carousel
        $sequence = 0;
        foreach ($datum as $d) {
            $newData['url'] = '';
            $newData['width'] = 0;
            $newData['height'] = 0;
            $newData['typeid'] = $d->getMediaType();
            if ($d->getMediaType() == 1) {// Photo
                $reap = $d->getImageVersions2()->getCandidates()[0];
            } elseif ($d->getMediaType() == 2) {// Video
                $reap = $d->getVideoVersions()[0];
            }
            $newData['url'] = $reap->getUrl();
            $newData['width'] = $reap->getWidth();
            $newData['height'] = $reap->getHeight();
            $newData['sequence'] = $sequence;
            $sequence++;
            $db->insert('wads', $newData);
        }
    }
}// .insertWad

function insertPost($db = null, $datum = null, $location_id = 1, $member_id = 1) {
    $check = $db->select('posts',
        ['id'],
        ['pk' => $datum->getPk(), 'active' => true]
    );

    if (count($check) > 0) {
        return $check[0]['id'];
    } else {// save location to db
        ($datum->hasCaption() && $datum->getCaption() !== null) ? $caption = $datum->getCaption()->getText() : $caption = '';
        //echo "New post with caption $caption";

        $db->insert('posts', 
            [
                'pk' => $datum->getPk(),
                'sourceid' => $datum->getId(),
                'location_id' => $location_id,
                'member_id' => $member_id,
                'typeid' => $datum->getMediaType(),
                'caption' => $caption,
                'likes' => $datum->getLikeCount(),
                'comments' => $datum->getCommentCount(),
                'takenat' => $datum->getTakenAt()
            ]
        );
        return $db->id();
    }
}// .function insertPost

function insertLocation($db = null, $location = null) {
    $check = $db->select('locations',
        ['id'],
        ['pk' => $location->getPk(), 'active' => true]
    );

    if (count($check) > 0) {
        return $check[0]['id'];
    } else {// save location to db
        //echo "New location named " . $location->getName() . "\n";
        ($location->hasFacebookPlacesId()) ? $fbplacesid = $location->getFacebookPlacesId() : $fbplacesid = 0;

        $db->insert('locations', 
            [
                'pk' => $location->getPk(),
                'lat' => $location->getLat(),
                'lng' => $location->getLng(),
                'shortname' => $location->getShortName(),
                'name' => $location->getName(),
                'address' => $location->getAddress(),
                'fbplacesid' => $fbplacesid

            ]
        );
        return $db->id();
    }
}// .function insertLocation
