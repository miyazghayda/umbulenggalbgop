<?php
require 'vendor/autoload.php';

use Medoo\Medoo;
use InstagramAPI\Instagram;
use InstagramAPI\Constants;
use InstagramAPI\Signatures;
use InstagramAPI\Exception;
use InstagramAPI\Media\Photo\InstagramPhoto;
use InstagramAPI\Media\Video\InstagramVideo;

require_once 'db.php';
/*$db = new Medoo([
    'database_type' => 'mysql',
    'database_name' => 'automateit',
    'server' => 'database',
    'username' => 'user',
    'password' => 'jayapura',
    'charset' => 'utf8'
]);*/

$reap_path = join(DIRECTORY_SEPARATOR, [
    '', 'var', 'www', 'html', 'automateit',
    'webroot', 'files', 'images', 'upload', '']);

$wad_path = join(DIRECTORY_SEPARATOR, [
    '', 'var', 'www', 'html', 'automateit2',
    'files', 'images', 'upload', '']);

while (true) {
    echo "File \033[34m" . basename(__FILE__, '.php') . "\033[0m Waiting for accounts to process on " . date('d-m-Y H:i') . PHP_EOL;

    echo "Waiting for account(s) to process on " . date('d-m-Y H:i') . "\n";
    // Gather all account_id(s) on both cargos and replicas tables
    $accountsOnCargo = $db->select('cargos',
    ['account_id'],
    [
        'AND' => [
            'typeid[!]' => 4,
            'schedule[<=]' => date('Y-m-d H:i'),
            'uploaded' => false,
            'active' => true
        ],
        'GROUP' => ['account_id']
    ]);
    $accountsOnReplica = $db->select('replicas',
    ['account_id'],
    [
        'AND' => [
            'schedule[<=]' => date('Y-m-d H:i'),
            'uploaded' => false,
            'active' => true
        ],
        'GROUP' => ['account_id']
    ]);

    // Remove redundant account_id
    $accountIds = [];
    foreach ($accountsOnCargo as $a) {
        if (!in_array($a['account_id'], $accountIds)) array_push($accountIds, $a['account_id']);
    }
    foreach ($accountsOnReplica as $a) {
        if (!in_array($a['account_id'], $accountIds)) array_push($accountIds, $a['account_id']);
    }

    // Check if account_id is paid account
    $paidAccountIds = [];
    foreach ($accountIds as $a) {
        $check = $db->count('accounts',
        [
            'AND' => [
                'id' => $a,
                'statusid' => 5,
                'active' => true,
            ]
        ]);
        if ($check > 0) {
            array_push($paidAccountIds, $a);
        }
    }

    if (count($paidAccountIds) > 0) {
        $ig = new Instagram(false, false);
        foreach ($paidAccountIds as $account_id) {
            $account = $db->select('accounts',
                ['[<]proxies' => ['proxy_id' => 'id']],
                [
                    'account' => [
                        'accounts.id(account_id)',
                        'accounts.user_id',
                        'accounts.username',
                        'accounts.password',
                        'accounts.proxy_id'
                    ],
                    'proxy' => [
                        'proxies.name AS proxy_name',
                    ]
                ],
                [
                    'AND' => [
                        'accounts.id' => $account_id
                    ],
                    'LIMIT' => 1
            ]);
            $account = $account[0];
            
            try {
                if ($account['account']['proxy_id'] > 1) $ig->setProxy($account['account']['proxy']['proxy_name']);
                echo 'Login with username ' . $account['account']['username'] . PHP_EOL;
                // Login akun IG
                $ig->login($account['account']['username'], $account['account']['password']);
                
                // Process cargos table, original content from account
                $cargos = $db->select('cargos',
                    [
                        'id(cargo_id)',
                        'schedule',
                        'caption',
                        'location_id'
                    ],
                    [
                        'AND' => [
                            'account_id' => $account['account']['account_id'],
                            'schedule[<=]' => date('Y-m-d H:i'),
                            'uploaded' => false,
                            'active' => true,
                        ],
                        'ORDER' => ['schedule' => 'ASC'],
                    ]
                );
                
                // Get contents (reaps) and upload to IG
                foreach ($cargos as $c) {
                    //uploadReap($db, $ig, $reap_path, $account, $c);
                    uploadData($db, $ig, 1, $reap_path, $account, $c);
                    sleep(rand(28, 38));
                }

                // Process replicas table, repost content from other account(s)
                $replicas = $db->select('replicas',
                    [
                        'id(replica_id)',
                        'schedule',
                        'caption',
                        'post_id',
                        'location_id'
                    ],
                    [
                        'AND' => [
                            'account_id' => $account['account']['account_id'],
                            'schedule[<=]' => date('Y-m-d H:i'),
                            'uploaded' => false,
                            'active' => true,
                        ],
                        'ORDER' => ['schedule' => 'ASC']
                    ]);

                    // Get contents (wads) and upload to IG
                    foreach ($replicas as $r) {
                        uploadData($db, $ig, 2, $wad_path, $account, $r);
                        sleep(rand(28, 38));
                        //uploadWad($db, $ig, $wad_path, $account, $r);
                    }
            } catch (\Exception $loginException) {
                echo $loginException->getMessage() . PHP_EOL;
            }// .try login
        }// .foreach paid account
        // Remove all files on wads directory
        $files = glob($wad_path . '*');
        foreach ($files as $f) {
            if (is_file($f)) unlink($f);
        }
    }// .if paid accounts more than 0
    sleep(60);
}// .while true

// $reapOrWad
// 1 = reap
// 2 = wad
function uploadData($db = null, $ig = null, $reapOrWad = 1, $path = null, $account = null, $data = null) {
    $location = $db->select('locations',
    [
        'id',
        'lat',
        'lng',
        'name'
    ],
    [
        'AND' => [
            'id' => $data['location_id']
        ],
        'LIMIT' => 1
    ]);
    $location = $location[0];
    
    // if content by user self
    if ($reapOrWad == 1) {
        $contents = $db->select('reaps', 
        [
            'id(reap_id)',
            'typeid',
            'extension',
            'sequence'
        ],
        [
            'AND' => [
                'cargo_id' => $data['cargo_id'],
                'active' => true
            ],
            'ORDER' => ['sequence' => 'ASC']
        ]);
    // if content by idol
    } elseif ($reapOrWad == 2) {

        $db->update('wads',
        ['active' => false],
        ['post_id' => $data['post_id']]);
        // redownload post to get valid url

        $post = $db->select('posts',
        ['sourceid'],
        ['id' => $data['post_id']]);
        $post = $post[0];

        try {
            $response = $ig->media->getInfo($post['sourceid']);
            $item = $response->getItems()[0];
            
            // Insert new Wad
            if ($data['post_id'] > 0 && $item->getMediaType() == 1) { // Photo
                insertWad($db, $item->getImageVersions2()->getCandidates()[0], $data['post_id'], $item->getMediaType());
                echo 'Getting photo' . PHP_EOL;
            } elseif ($data['post_id'] > 0 && $item->getMediaType() == 2) { // Video
                insertWad($db, $item->getVideoVersions()[0], $data['post_id'], $item->getMediaType());
                echo 'Getting video' . PHP_EOL;
            } elseif ($data['post_id'] > 0 && $item->getMediaType() == 8) { // Carousel
                insertWad($db, $item->getCarouselMedia(), $data['post_id'], $item->getMediaType());
                echo 'Getting carousel' . PHP_EOL;
            }
        } catch (\Exception $e) {
            $db->update('replicas',
                ['uploaded' => true, 'modified' => date('Y-m-d H:i:s')],
                ['id' => $data['replica_id']]);
        }// .try to get media from ig server

        $contents = $db->select('wads',
            [
                'id(wad_id)',
                'typeid',
                'url',
                'sequence'
            ],
            [
                'AND' => [
                    'post_id' => $data['post_id'],
                    'active' => true,
                ],
                'ORDER' => ['sequence' => 'ASC']
            ]);
    }

    // Single content
    if (count($contents) == 1) {
        echo 'Upload content with caption ' . $data['caption'] . PHP_EOL;
        try {
            if ($reapOrWad == 1) {
                // photo
                if ($contents[0]['typeid'] == 1) {
                    $content = new InstagramPhoto($path . $contents[0]['reap_id'] . '.' . $contents[0]['extension']);
                } elseif ($contents[0]['typeid'] == 2) {
                // video
                    $content = new InstagramVideo($path . $contents[0]['reap_id'] . '.' . $contents[0]['extension']);
                }
            } elseif ($reapOrWad == 2) {
                // First, download from server
                $filePath = $path . $contents[0]['wad_id'];
                if ($contents[0]['typeid'] == 1) {
                    $filePath = $filePath . '.jpg';
                } elseif ($contents[0]['typeid'] == 2) {
                    $filePath = $filePath . '.mp4';
                }

                echo 'Now download from ' . $contents[0]['url'] . PHP_EOL;
                
                set_time_limit(0);
                $fp = fopen ($filePath, 'w+');
                $ch = curl_init(str_replace(' ', '%20', $contents[0]['url']));
                curl_setopt($ch, CURLOPT_FILE, $fp);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                curl_exec($ch);
                curl_close($ch);
                fclose($fp);
                
                if ($contents[0]['typeid'] == 1) {
                    $content = new InstagramPhoto($filePath);
                } elseif ($contents[0]['typeid'] == 2) {
                    $content = new InstagramVideo($filePath);
                }
            }
            $metadata = [];
            $metadata['caption'] = $data['caption'];
            if ($data['location_id'] > 1) {
                $loc = null;
                try {
                    $loc = $ig->location->search($location['lat'], $location['lng'])->getVenues()[0];
                } catch (\Exception $e) {
                    echo $e->getMessage() . PHP_EOL;
                }
                if ($loc !== null) {
                    $metadata['location'] = $loc;
                }
            }
            if ($contents[0]['typeid'] == 1) {
                $uploading = $ig->timeline->uploadPhoto($content->getFile(), $metadata);
            } elseif ($contents[0]['typeid'] == 2) {
                $uploading = $ig->timeline->uploadVideo($content->getFile(), $metadata);
            }

            if ($uploading->getStatus() == 'ok') {
                if ($reapOrWad == 1) {
                    $db->update('cargos',
                        ['uploaded' => true, 'modified' => date('Y-m-d H:i:s')],
                        ['id' => $data['cargo_id']]);
                } elseif ($reapOrWad == 2) {
                    $db->update('replicas',
                        ['uploaded' => true, 'modified' => date('Y-m-d H:i:s')],
                        ['id' => $data['replica_id']]);
                }
                return true;
            } 
            echo 'Fail to upload' . PHP_EOL;
        } catch (\Exception $uploadException) {
            if ($reapOrWad == 1) {
                // update cargos
                $db->update('cargos',
                    ['active' => false],
                    ['id' => $data['cargo_id']]);
            } elseif ($reapOrWad == 2) {
                // update replicas
                $db->update('replicas',
                    ['active' => false],
                    ['id' => $data['replica_id']]);
            }

            echo $uploadException->getMessage() . PHP_EOL;
        }// .try upload content
    // Multiple contents
    } elseif (count($contents) > 1) {
        echo 'Upload album with caption ' . $data['caption'] . PHP_EOL;
       
        // Preparing album to upload
        $media = [];
        if ($reapOrWad == 1) {
            foreach($contents as $content) {
                if ($content['typeid'] == 1) array_push($media, ['type' => 'photo', 'file' => $path . $content['reap_id'] . '.' . $content['extension']]);
                if ($content['typeid'] == 2) array_push($media, ['type' => 'video', 'file' => $path . $content['reap_id'] . '.' . $content['extension']]);
            }
        } elseif ($reapOrWad == 2) {
            foreach ($contents as $content) {
                // First, download from server
                $filePath = $path . $content['wad_id'];
                if ($content['typeid'] == 1) {
                    $filePath = $filePath . '.jpg';
                } elseif ($content['typeid'] == 2) {
                    $filePath = $filePath . '.mp4';
                }

                set_time_limit(0);
                $fp = fopen ($filePath, 'w+');
                $ch = curl_init(str_replace(' ', '%20', $content['url']));
                curl_setopt($ch, CURLOPT_FILE, $fp);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                curl_exec($ch);
                curl_close($ch);
                fclose($fp);
                if ($content['typeid'] == 1) array_push($media, ['type' => 'photo', 'file' => $filePath]);
                if ($content['typeid'] == 2) array_push($media, ['type' => 'video', 'file' => $filePath]);
            }// .foreach contents
        }

        $mediaOptions = ['targetFeed' => Constants::FEED_TIMELINE_ALBUM];

        foreach($media as &$item) {
            $validMedia = null;
            switch ($item['type']) {
            case 'photo':
                $validMedia = new InstagramPhoto($item['file'], $mediaOptions);
                break;
            case 'video':
                $validMedia = new InstagramVideo($item['file'], $mediaOptions);
                break;
            default:

            }
            if ($validMedia === null) {
                continue;
            }

            try {
                $item['file'] = $validMedia->getFile();
                $item['__media'] = $validMedia;
            } catch(\Exception $e) {
                continue;
            }
            if (!isset($mediaOptions['forceAspectRatio'])) {
                $mediaDetails = $validMedia instanceof InstagramPhoto
                    ? new \InstagramAPI\Media\Photo\PhotoDetails($item['file'])
                    : new \InstagramAPI\Media\Video\VideoDetails($item['file']);
                $mediaOptions['forceAspectRatio'] = $mediaDetails->getAspectRatio();
            }
        }// .foreach media

        // Upload album
        try {
            $uploading = $ig->timeline->uploadAlbum($media, ['caption' => $data['caption']]);
            if ($uploading->getStatus() == 'ok') {
                if ($reapOrWad == 1) {
                $db->update('cargos',
                    ['uploaded' => true, 'modified' => date('Y-m-d H:i:s')],
                    ['id' => $cargo['cargo']['cargo_id']]);
                } elseif ($reapOrWad == 2) {
                    $db->update('replicas',
                        ['uploaded' => true, 'modified' => date('Y-m-d H:i:s')],
                        ['id' => $data['replica_id']]);
                }
                return true;
            }
        } catch (\Exception $uploadException) {
            echo $uploadException->getMessage() . PHP_EOL;
            if ($reapOrWad == 1) {
                // update cargos
                $db->update('cargos',
                    ['active' => false],
                    ['id' => $data['cargo_id']]);
            } elseif ($reapOrWad == 2) {
                // update replicas
                $db->update('replicas',
                    ['active' => false],
                    ['id' => $data['replica_id']]);
            }
        }// .try upload contents
    }
}

function insertWad($db = null, $datum = null, $post_id = 1, $typeid = 1)
{
    $newData = [
        'post_id' => $post_id,
        'typeid' => $typeid,
        'sequence' => 0,
    ];

    if ($typeid == 1 || $typeid == 2) { // Single photo or video
        $check = $db->select('wads',
            ['id'],
            ['post_id' => $post_id, 'url' => $datum->getUrl(), 'active' => true]
        );

        if (count($check) < 1) {
            $newData['url'] = $datum->getUrl();
            $newData['width'] = $datum->getWidth();
            $newData['height'] = $datum->getHeight();
            $db->insert('wads', $newData);
            return $db->id();
        } else {
            return $check[0]['id'];
        }
    } elseif ($typeid == 8) { // Carousel
        $sequence = 0;
        foreach ($datum as $d) {
            $newData['url'] = '';
            $newData['width'] = 0;
            $newData['height'] = 0;
            $newData['typeid'] = $d->getMediaType();
            if ($d->getMediaType() == 1) { // Photo
                $reap = $d->getImageVersions2()->getCandidates()[0];
            } elseif ($d->getMediaType() == 2) { // Video
                $reap = $d->getVideoVersions()[0];
            }
            $check = $db->select('wads',
                ['id'],
                ['post_id' => $post_id, 'url' => $reap->getUrl(), 'active' => true]
            );

            if (count($check) < 1) {
                $newData['url'] = $reap->getUrl();
                $newData['width'] = $reap->getWidth();
                $newData['height'] = $reap->getHeight();
                $newData['sequence'] = $sequence;
                $sequence++;
                $db->insert('wads', $newData);
            }
        }
    }
} // .insertWad
