<?php
require 'vendor/autoload.php';

use Medoo\Medoo;
use InstagramAPI\Instagram;
use InstagramAPI\Constants;
use InstagramAPI\Signatures;
use InstagramAPI\Exception;

require_once 'db.php';
/*$db = new Medoo([
    'database_type' => 'mysql',
    'database_name' => 'automateit',
    'server' => 'database',
    'username' => 'user',
    'password' => 'jayapura',
    'charset' => 'utf8'
]);*/

// Get batch to process
$batch = 0;
if (isset($argv[1])) $batch = $argv[1];

while (true) {
    echo "File \033[34m" . basename(__FILE__, '.php') . "\033[0m Waiting for accounts to process on " . date('d-m-Y H:i') . PHP_EOL;

    $ig = new Instagram(false, false);

    echo "Waiting for account(s) to process on " . date('d-m-Y H:i') . "\n";

    $accounts = $db->select('accounts',
        [
            '[>]preferences' => ['id' => 'account_id'],
            '[<]proxies' => ['proxy_id' => 'id']
        ],
        [
            'account' => [
                'accounts.id(account_id)',
                'accounts.user_id',
                'accounts.username',
                'accounts.password'
            ],
            'preference' => [
                'preferences.maxunfollowperday',
                'preferences.unfollowtoday',
            ],
            'proxy' => [
                'proxies.id AS proxy_id',
                'proxies.name AS proxy_name'
            ]
        ],
        [
            'AND' => [
                'accounts.statusid' => 5,
                'accounts.active' => true,
                'preferences.unfollowbydefault' => true,
            ]
        ]
    );
        
    foreach ($accounts as $a) {
        $accountmaxunfollow = $a['preference']['maxunfollowperday'];
        $accounthadunfollowingfor = $a['preference']['unfollowtoday'];

        if ($accountmaxunfollow > 0 && $accounthadunfollowingfor < $accountmaxunfollow) {
            $followings = $db->select('followinglists',
            [
                '[<]members' => ['member_id' => 'id']
            ],
            [
                'followinglist' => [
                    'followinglists.id(followinglist_id)',
                    'followinglists.vassal_id',
                    'followinglists.fellow_id',
                ],
                'member' => [
                    'members.id(member_id)',
                    'members.pk',
                    'members.username',
                ]
            ],
            [
                'AND' => [
                    'followinglists.account_id' => $a['account']['account_id'],
                    'followinglists.followed' => true,
                    'followinglists.unfollowed' => false,
                    'followinglists.who' => false,
                    'followinglists.active' => true
                ],
                'ORDER' => ['followedat' => 'ASC']
            ]);

            if (count($followings) > 0) {
                // Login to IG
                try {
                    if ($a['proxy']['id'] > 1) $ig->setProxy($a['proxy']['name']);
                    $igLogin = $ig->login($a['account']['username'], $a['account']['password']);
                    echo "Succeed to login to " . $a['account']['username'] . PHP_EOL;
                    $i = $accounthadunfollowingfor;
                    foreach ($followings as $f) {
                        if ($i < $accountmaxunfollow) {
                            try {
                                echo $i . ' unfollowing ' . $f['member']['username'] . PHP_EOL;
                                $unfollow = $ig->people->unfollow($f['member']['pk']);
                                
                                if ($unfollow->getStatus() == 'ok') {
                                    // update followinglists table
                                    $db->update('followinglists',
                                    ['unfollowed' => true, 'unfollowedat' => date('Y-m-d H:i:s'), 'who' => false, 'active' => true],
                                    ['member_id' => $f['member']['member_id'], 'account_id' => $a['account']['account_id']]);
                                    sleep(rand(28, 38));
                                }
                            } catch (\Exception $unfollowExcept) {
                                // update followinglists table no matter what happen next
                                $db->update('followinglists',
                                ['who' => false, 'active' => false, 'note' => 'Error, no connection between'],
                                ['member_id' => $f['member']['member_id'], 'account_id' => $a['account']['account_id']]);
                                echo $unfollowExcept->getMessage() . PHP_EOL;
                            }
                        }// .if unfollow below the max unfollow
                        $i++;
                    }
                    // update preferences table 
                    $db->update('preferences', ['unfollowtoday' => $i], ['account_id' => $a['account']['account_id']]);
                } catch (\Exception $e) {
                    echo $e . PHP_EOL;
                }// .try login ig
            }// .if followings > 0
        }
    }// .foreach accounts
    // Sleep foreach process
    sleep(1800);
}// .while true