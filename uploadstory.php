<?php
require 'vendor/autoload.php';

use Medoo\Medoo;
use InstagramAPI\Instagram;
use InstagramAPI\Constants;
use InstagramAPI\Signatures;
use InstagramAPI\Exception;
use InstagramAPI\Media\Photo\InstagramPhoto;
use InstagramAPI\Media\Video\InstagramVideo;

require_once 'db.php';
/*$db = new Medoo([
    'database_type' => 'mysql',
    'database_name' => 'automateit',
    'server' => 'database',
    'username' => 'user',
    'password' => 'jayapura',
    'charset' => 'utf8'
]);*/

$reap_path = join(DIRECTORY_SEPARATOR, [
    '', 'var', 'www', 'html', 'automateit',
    'webroot', 'files', 'images', 'upload', '']);

while (true) {
    //echo "Waiting for account(s) to process on " . date('d-m-Y H:i') . "\n";
    echo "File \033[34m" . basename(__FILE__, '.php') . "\033[0m Waiting for account(s) to process on " . date('d-m-Y H:i') . PHP_EOL;
   
    $accountsOnCargo = $db->select('cargos',
    ['account_id'],
    [
        'AND' => [
            'typeid' => 4,
            'schedule[<=]' => date('Y-m-d H:i'),
            'uploaded' => false,
            'active' => true
        ],
        'GROUP' => ['account_id']
    ]);

    // Remove redundant account_id
    $accountIds = [];
    foreach ($accountsOnCargo as $a) {
        if (!in_array($a['account_id'], $accountIds)) array_push($accountIds, $a['account_id']);
    }
    // Check if account_id is paid account
    $paidAccountIds = [];
    foreach ($accountIds as $a) {
        $check = $db->count('accounts',
        [
            'AND' => [
                'id' => $a,
                'statusid' => 5,
                'active' => true,
            ]
        ]);
        if ($check > 0) {
            array_push($paidAccountIds, $a);
        }
    }

    if (count($paidAccountIds) > 0) {
        $ig = new Instagram(false, false);
        foreach ($paidAccountIds as $account_id) {
            $account = $db->select('accounts',
                ['[<]proxies' => ['proxy_id' => 'id']],
                [
                    'account' => [
                        'accounts.id(account_id)',
                        'accounts.user_id',
                        'accounts.username',
                        'accounts.password',
                        'accounts.proxy_id'
                    ],
                    'proxy' => [
                        'proxies.name AS proxy_name',
                    ]
                ],
                [
                    'AND' => [
                        'accounts.id' => $account_id
                    ],
                    'LIMIT' => 1
            ]);
            $account = $account[0];
            
            try {
                if ($account['account']['proxy_id'] > 1) $ig->setProxy($account['account']['proxy']['proxy_name']);
                echo 'Login with username ' . $account['account']['username'] . PHP_EOL;
                // Login akun IG
                $ig->login($account['account']['username'], $account['account']['password']);
                
                // Process cargos table, original content from account
                $cargos = $db->select('cargos',
                    [
                        'id(cargo_id)',
                        'schedule',
                        'caption',
                        'question',
                        'answery',
                        'answern',
                        'hashtags',
                    ],
                    [
                        'AND' => [
                            'typeid' => 4,
                            'account_id' => $account['account']['account_id'],
                            'schedule[<=]' => date('Y-m-d H:i'),
                            'uploaded' => false,
                            'active' => true,
                        ],
                        'ORDER' => ['schedule' => 'ASC'],
                    ]
                );
                
                // Get contents (reaps) and upload to IG
                foreach ($cargos as $c) {
                    //uploadReap($db, $ig, $reap_path, $account, $c);
                    uploadData($db, $ig, $reap_path, $account, $c);

                    $db->update('cargos',
                    ['uploaded' => true, 'modified' => date('Y-m-d H:i:s')],
                    ['id' => $c['cargo_id']]);
                    sleep(rand(28, 38));
                }
            } catch (\Exception $loginException) {
                echo $loginException->getMessage() . PHP_EOL;
            }// .try login
        }// .foreach paid account
    }// .if paid accounts more than 0
    sleep(60);
}// .while true

function uploadData($db = null, $ig = null, $path = null, $account = null, $data = null) {
    $contents = $db->select('reaps', 
    [
        'id(reap_id)',
        'typeid',
        'extension',
        'sequence'
    ],
    [
        'AND' => [
            'cargo_id' => $data['cargo_id'],
            'active' => true
        ],
        'ORDER' => ['sequence' => 'ASC']
    ]);

    // Single content
    echo 'Upload content with caption ' . $data['caption'] . PHP_EOL;
    try {
        $hashtagText1 = $data['caption'];// 22 fonts maximum
        $hashtagText2 = '#' . $hashtagText1;
        
        $questionText = $data['question']; // 24 fonts maximum per line, maximum 5 lines (120 chars)
        $wordwrapQuestionText = wordwrap($questionText, 24, '6969');
        $questionLines = explode('6969', $wordwrapQuestionText);
        $answer1 = $data['answery'];
        $answer2 = $data['answern'];
        
        $fontFile = __DIR__ . '/System San Francisco Display Bold.ttf';
        $fontColor = ['red' => 244, 'green' => 81, 'blue' => 81, 'alpha' => 1];
        $metadata = [];
        // If only question
        if (empty($data['caption']) && !empty($data['question'])) {
            $pollsY = onlyQuestion($fontFile, $path . $contents[0]['reap_id'] . '.' . $contents[0]['extension'], $questionLines);
            
            $metadata = [
                'story_polls' => [
                [
                    'question'          => $questionText,
                    'viewer_vote'       => 0,
                    'viewer_can_vote'   => true,
                    'tallies'           => [
                        [
                            'text'      => $answer1,
                            'count'     => 0,
                            'font_size' => 21.0,
                        ],
                        [
                            'text'      => $answer2,
                            'count'     => 0,
                            'font_size' => 21.0,
                        ],
                    ],
                    'x'                 => 0.5,
                    'y'                 => $pollsY,
                    'width'             => 0.5661107,
                    'height'            => 0.10647108,
                    'rotation'          => 0.0,
                    'is_sticker'        => true,
                ],
                ]];
        } elseif (!empty($data['caption']) && empty($data['question'])) {
        // If only caption
            if ($contents[0]['typeid'] == 1) onlyCaption($fontFile, $path . $contents[0]['reap_id'] . '.' . $contents[0]['extension'], $data['caption']);
            
            $metadata = [
                // (optional) Captions can always be used, like this:
                'caption'  => $hashtagText2,
                // (optional) To add a hashtag, do this:
                'hashtags' => [
                    // Note that you can add more than one hashtag in this array.
                    [
                        'tag_name'         => $hashtagText1, // Hashtag WITHOUT the '#'! NOTE: This hashtag MUST appear in the caption.
                        'x'                => 0.5, // Range: 0.0 - 1.0. Note that x = 0.5 and y = 0.5 is center of screen.
                        'y'                => 0.5, // Also note that X/Y is setting the position of the CENTER of the clickable area.
                        'width'            => 0.8, // Clickable area size, as percentage of image size: 0.0 - 1.0
                        'height'           => 0.4, // ...
                        'rotation'         => 0.0,
                        'is_sticker'       => false, // Don't change this value.
                        'use_custom_title' => false, // Don't change this value.
                    ],
                ],
            ];
        } elseif (!empty($data['caption']) && !empty($data['question'])) {
            // If caption and question
            if ($contents[0]['typeid'] == 1) $pollsY = captionQuestion($fontFile, $path . $contents[0]['reap_id'] . '.' . $contents[0]['extension'], $data['caption'], $questionLines);
            
            $metadata = [
                // (optional) Captions can always be used, like this:
                'caption'  => $hashtagText2,
                // (optional) To add a hashtag, do this:
                'hashtags' => [
                    // Note that you can add more than one hashtag in this array.
                    [
                        'tag_name'         => $hashtagText1, // Hashtag WITHOUT the '#'! NOTE: This hashtag MUST appear in the caption.
                        'x'                => 0.5, // Range: 0.0 - 1.0. Note that x = 0.5 and y = 0.5 is center of screen.
                        'y'                => 0.1, // Also note that X/Y is setting the position of the CENTER of the clickable area.
                        'width'            => 0.8, // Clickable area size, as percentage of image size: 0.0 - 1.0
                        'height'           => 0.4, // ...
                        'rotation'         => 0.0,
                        'is_sticker'       => false, // Don't change this value.
                        'use_custom_title' => false, // Don't change this value.
                    ],
                ],
                'story_polls' => [
                    [
                        'question'          => $questionText,
                        'viewer_vote'       => 0,
                        'viewer_can_vote'   => true,
                        'tallies'           => [
                            [
                                'text'      => $answer1,
                                'count'     => 0,
                                'font_size' => 21.0,
                            ],
                            [
                                'text'      => $answer2,
                                'count'     => 0,
                                'font_size' => 21.0,
                            ],
                        ],
                        'x'                 => 0.5,
                        'y'                 => $pollsY,
                        'width'             => 0.5661107,
                        'height'            => 0.10647108,
                        'rotation'          => 0.0,
                        'is_sticker'        => true,
                    ]
                ],
            ];
        } /*elseif (empty($data['caption']) && empty($data['question'])) {
            // If no caption and question
            //$metadata = [];
        }*/
        if (!empty($data['hashtags'])) {
            //$metadata['caption'];
            if (!array_key_exists('caption', $metadata)) {
                $metadata['caption'] = '';
                $metadata['hashtags'] = [];
            }
            //$hashtags = explode(',', $data['hashtags']);
            //$y = 0.1;
            //$i = 1;
            //foreach ($hashtags as $h) {
            $metadata['caption'] = $metadata['caption'] . ' #' . $data['hashtags'];
            $metadata['caption'] = trim($metadata['caption']);
            array_push($metadata['hashtags'], [
                'tag_name'         => $data['hashtags'], // Hashtag WITHOUT the '#'! NOTE: This hashtag MUST appear in the caption.
                'x'                => 0.5, // Range: 0.0 - 1.0. Note that x = 0.5 and y = 0.5 is center of screen.
                'y'                => 0.5, // Also note that X/Y is setting the position of the CENTER of the clickable area.
                'width'            => 0.8, // Clickable area size, as percentage of image size: 0.0 - 1.0
                'height'           => 0.4, // ...
                'rotation'         => 0.0,
                'is_sticker'       => false, // Don't change this value.
                'use_custom_title' => false, // Don't change this value.
            ]);
                //$i++;
            //}
        }

        try {
            print_r($metadata);
            // photo
            if ($contents[0]['typeid'] == 1) {
                $imageOriginal = $path . $contents[0]['reap_id'] . '.' . $contents[0]['extension'];
                $imageCopy = $path . $contents[0]['reap_id'] . '_copy.' . $contents[0]['extension'];

                if (file_exists($imageCopy)) {
                    $imgPath = $imageCopy;
                } else {
                    $imgPath = $imageOriginal;
                }
                $photo = new InstagramPhoto($imgPath, ['targetFeed' => \InstagramAPI\Constants::FEED_STORY]);
                $ig->story->uploadPhoto($photo->getFile(), $metadata);
                // Remove photo copy
                if (file_exists($imageCopy) && is_file($imageCopy)) unlink($imageCopy);
            } else {
            // video
                $video = new InstagramVideo($path . $contents[0]['reap_id'] . '.' . $contents[0]['extension'], ['targetFeed' => \InstagramAPI\Constants::FEED_STORY]);
                $ig->story->uploadVideo($video->getFile(), $metadata);
            }
        } catch (\Exception $e) {
            echo 'Something went wrong: '.$e->getMessage()."\n";
        }
    } catch (\Exception $uploadException) {
        echo $uploadException->getMessage() . PHP_EOL;
    }// .try upload content
}

function captionQuestion($fontFile, $photoFilename, $caption, $questionLines) {
    $fontFile = __DIR__ . '/System San Francisco Display Bold.ttf';
    $fontColor = ['red' => 244, 'green' => 81, 'blue' => 81, 'alpha' => 1];

    try {
        $hashtagText1 = $caption;// 22 fonts maximum
        $hashtagText2 = '#' . $hashtagText1;

        $image = new \claviska\SimpleImage($photoFilename);
        $pos = null;
        $image->text($hashtagText2, [
            'fontFile' => $fontFile,
            'size' => 72,
            'anchor' => 'top',
            'yOffset' => 25,
        ],
        $pos
        );
        $image->roundedRectangle(
            $pos['x1'] - 15,
            $pos['y1'] - 15,
            $pos['x2'] + 15,
            $pos['y2'] + 15,
            20,
            'white',
            'filled'
        );
        $image->text($hashtagText2, [
            'fontFile' => $fontFile,
            'size' => 72,
            'color' => $fontColor,
            'anchor' => 'top',
            'yOffset' => 25,
        ],
        $pos
        );
        $i = 0;
        $pollsYOffset = 50;
        foreach ($questionLines as $q) {
            $image->text($q, [
                'fontFile' => $fontFile,
                'size' => 64,
                'color' => 'white',
                'anchor' => 'center',
                'yOffset' => $i * $pollsYOffset
            ]);
            $i++;
        }
        $pollsY = 0.5 + ($i * 0.065);
        $photoPath = pathinfo($photoFilename);
        $image->toFile($photoPath['dirname'] . '/' . $photoPath['filename'] . '_copy.' . $photoPath['extension']);
        return $pollsY;
    } catch (\claviska\SimpleImage\Exception $err) {
        echo $err->getMessage();
    }
}

function onlyCaption($fontFile, $photoFilename, $caption) {
    $fontFile = __DIR__ . '/System San Francisco Display Bold.ttf';
    $fontColor = ['red' => 244, 'green' => 81, 'blue' => 81, 'alpha' => 1];

    try {
        $hashtagText1 = $caption;// 22 fonts maximum
        $hashtagText2 = '#' . $hashtagText1;

        $image = new \claviska\SimpleImage($photoFilename);
        $pos = null;
        $image->text($hashtagText2, [
            'fontFile' => $fontFile,
            'size' => 72,
            'anchor' => 'center',
        ],
        $pos
        );
        $image->roundedRectangle(
            $pos['x1'] - 15,
            $pos['y1'] - 15,
            $pos['x2'] + 15,
            $pos['y2'] + 15,
            20,
            'white',
            'filled'
        );
        $image->text($hashtagText2, [
            'fontFile' => $fontFile,
            'size' => 72,
            'color' => $fontColor,
            'anchor' => 'center',
        ],
        $pos
        );
        $photoPath = pathinfo($photoFilename);
        $image->toFile($photoPath['dirname'] . '/' . $photoPath['filename'] . '_copy.' . $photoPath['extension']);
    } catch (\claviska\SimpleImage\Exception $err) {
        echo $err->getMessage();
    }
}

function onlyQuestion($fontFile, $photoFilename, $questionLines) {
    try {
        $image = new \claviska\SimpleImage($photoFilename);
        $pos = null;
        $i = 0;
        $pollsYOffset = 50;
        foreach ($questionLines as $q) {
            $image->text($q, [
                'fontFile' => $fontFile,
                'size' => 64,
                'color' => 'white',
                'anchor' => 'center',
                'yOffset' => $i * $pollsYOffset
            ]);
            $i++;
        }
        $pollsY = 0.5 + ($i * 0.065);
        $photoPath = pathinfo($photoFilename);
        $image->toFile($photoPath['dirname'] . '/' . $photoPath['filename'] . '_copy.' . $photoPath['extension']);
        return $pollsY;
    } catch (\claviska\SimpleImage\Exception $err) {
        echo $err->getMessage();
    }
}