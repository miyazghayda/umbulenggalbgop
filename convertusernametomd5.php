<?php
require 'vendor/autoload.php';

use InstagramAPI\Instagram;
use Medoo\Medoo;

$db = new Medoo([
    'database_type' => 'mysql',
    'database_name' => 'automateit',
    'server' => 'database',
    'username' => 'user',
    'password' => 'jayapura',
    'charset' => 'utf8',
]);

// Get batch to process
$username = '';
if (isset($argv[1])) {
    $username = $argv[1];
}

if (empty($username)) {
    $accounts = $db->select('accounts',['id', 'username', 'messy'],['active' => true]);
    $i = 1;
    foreach ($accounts as $account) {
        echo $i . '. ' . $account['username'] . ' ' . md5($account['username']) . PHP_EOL;
        $i++;
        if (!empty($account['messy'])) $db->update('accounts', ['messy' => md5($account['username'])], ['id' => $account['id']]);
    }
} else {
    $account = $db->get('accounts', ['id', 'username', 'messy'],['username' => $username]);
    echo $account['username'] . ' ' . md5($account['username']) . PHP_EOL;
    if (!empty($account['messy'])) $db->update('accounts', ['messy' => md5($account['username'])], ['id' => $account['id']]);
}