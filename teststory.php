<?php
require 'vendor/autoload.php';

use Medoo\Medoo;
use InstagramAPI\Instagram;
use InstagramAPI\Constants;
use InstagramAPI\Signatures;
use InstagramAPI\Exception;
use claviska\SimpleImage;

$username = 'miyazghayda';
$password = 'jayapura';
$debug = true;
$truncatedDebug = false;
$hashtagText1 = 'Balon';// 22 fonts maximum
$hashtagText2 = '#' . $hashtagText1;
$answer1 = 'Ada';
$answer2 = 'Ga';
$fontFile = __DIR__ . '/System San Francisco Display Bold.ttf';
$fontColor = ['red' => 244, 'green' => 81, 'blue' => 81, 'alpha' => 1];
$questionText = 'ada tradisi unik khas lebaran di kampungmu?'; // 24 fonts maximum per line, maximum 5 lines (120 chars)
$wordwrapQuestionText = wordwrap($questionText, 24, '6969');
//echo $wordwrapQuestionText;
$questionLines = explode('6969', $wordwrapQuestionText);

// polls
// each row question (max 24 chars) is equal to 0.1 y on instagram
//////////////////////
/////// MEDIA ////////
$photoFilename = 'images/6.jpg';
$photoToUpload = 'images/6_1.jpg';

try {
    $image = new \claviska\SimpleImage($photoFilename);
    $pos = null;
    $image->text($hashtagText2, [
        'fontFile' => $fontFile,
        'size' => 72,
        'anchor' => 'top',
        'yOffset' => 25,
    ],
    $pos
    );
    $image->roundedRectangle(
        $pos['x1'] - 15,
        $pos['y1'] - 15,
        $pos['x2'] + 15,
        $pos['y2'] + 15,
        20,
        'white',
        'filled'
    );
    $image->text($hashtagText2, [
        'fontFile' => $fontFile,
        'size' => 72,
        'color' => $fontColor,
        'anchor' => 'top',
        'yOffset' => 25,
    ],
    $pos
    );
    /*$image->text($questionText, [
        'fontFile' => $fontFile,
        'size' => 64,
        'color' => 'white',
        'anchor' => 'center'
    ]);*/
    $i = 0;
    $pollsYOffset = 50;
    foreach ($questionLines as $q) {
        $image->text($q, [
            'fontFile' => $fontFile,
            'size' => 64,
            'color' => 'white',
            'anchor' => 'center',
            'yOffset' => $i * $pollsYOffset
        ]);
        $i++;
    }
    $pollsY = 0.5 + ($i * 0.065);
    $image->toFile($photoToUpload, 'image/jpeg');
    //print_r($pos);
} catch (Exception $err) {
    echo $err->getMessage();
}

//////////////////////
$ig = new Instagram(false, false);
try {
    $ig->login($username, $password);
} catch (\Exception $e) {
    echo 'Something went wrong: '.$e->getMessage()."\n";
    exit(0);
}
// You don't have to provide hashtags or locations for your story. It is
// optional! But we will show you how to do both...
// NOTE: This code will make the hashtag area 'clickable', but YOU need to
// manually draw the hashtag or a sticker-image on top of your image yourself
// before uploading, if you want the tag to actually be visible on-screen!
// NOTE: The same thing happens when a location sticker is added. And the
// "location_sticker" WILL ONLY work if you also add the "location" as shown
// below.
// NOTE: And "caption" will NOT be visible either! Like all the other story
// metadata described above, YOU must manually draw the caption on your image.
// If we want to attach a location, we must find a valid Location object first:
/*try {
    //$location = $ig->location->search('112.7510', '-7.2492')->getVenues()[0];

    $location = $ig->location->search('109.629666', '-7.713944')->getVenues();
    print_r($location);
    //$location = $ig->location->findPlaces('delta mandala');
    //print_r($location);
    //$location = $ig->location->findPlacesNearby('109.629666', '-7.713944');
    if ($location->getStatus() == 'ok') {
        print_r($location);
        //$loc = $ig->location->search($location->getItems()[0]->getLocation()->getLng(), $location->getItems()[0]->getLocation()->getLat())->getVenues()[0];
        //print_r($loc->getVenues());
        //print_r($location->getItems()[0]->getLocation()->getLat());
    }
} catch (\Exception $e) {
    echo 'Something went wrong: '.$e->getMessage()."\n";
}*/
// Now create the metadata array:
$metadata = [
    // (optional) Captions can always be used, like this:
    'caption'  => $hashtagText2,
    // (optional) To add a hashtag, do this:
    'hashtags' => [
        // Note that you can add more than one hashtag in this array.
        [
            'tag_name'         => $hashtagText1, // Hashtag WITHOUT the '#'! NOTE: This hashtag MUST appear in the caption.
            'x'                => 0.5, // Range: 0.0 - 1.0. Note that x = 0.5 and y = 0.5 is center of screen.
            'y'                => 0.1, // Also note that X/Y is setting the position of the CENTER of the clickable area.
            'width'            => 0.8, // Clickable area size, as percentage of image size: 0.0 - 1.0
            'height'           => 0.4, // ...
            'rotation'         => 0.0,
            'is_sticker'       => false, // Don't change this value.
            'use_custom_title' => false, // Don't change this value.
        ],
        // ...
    ],
    'story_polls' => [
        [
            'question'          => $questionText,
            'viewer_vote'       => 0,
            'viewer_can_vote'   => true,
            'tallies'           => [
                [
                    'text'      => $answer1,
                    'count'     => 0,
                    'font_size' => 21.0,
                ],
                [
                    'text'      => $answer2,
                    'count'     => 0,
                    'font_size' => 21.0,
                ],
            ],
            'x'                 => 0.5,
            'y'                 => $pollsY,
            'width'             => 0.5661107,
            'height'            => 0.10647108,
            'rotation'          => 0.0,
            'is_sticker'        => true,
        ]
    ],
    // (optional) To add a location, do BOTH of these:
    /*'location_sticker' => [
        'width'         => 0.89333333333333331,
        'height'        => 0.071281859070464776,
        'x'             => 0.5,
        'y'             => 0.2,
        'rotation'      => 0.0,
        'is_sticker'    => true,
        'location_id'   => $loc->getExternalId(),
    ],
    'location' => $loc,*/
    // (optional) You can use story links ONLY if you have a business account with >= 10k followers.
    // 'link' => 'https://github.com/mgp25/Instagram-API',
];
try {
    // This example will upload the image via our automatic photo processing
    // class. It will ensure that the story file matches the ~9:16 (portrait)
    // aspect ratio needed by Instagram stories. You have nothing to worry
    // about, since the class uses temporary files if the input needs
    // processing, and it never overwrites your original file.
    //
    // Also note that it has lots of options, so read its class documentation!
    $photo = new \InstagramAPI\Media\Photo\InstagramPhoto($photoToUpload, ['targetFeed' => \InstagramAPI\Constants::FEED_STORY]);
    $ig->story->uploadPhoto($photo->getFile(), $metadata);
    // NOTE: Providing metadata for story uploads is OPTIONAL. If you just want
    // to upload it without any tags/location/caption, simply do the following:
    // $ig->story->uploadPhoto($photo->getFile());
} catch (\Exception $e) {
    echo 'Something went wrong: '.$e->getMessage()."\n";
}