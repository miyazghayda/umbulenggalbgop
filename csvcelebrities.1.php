<?php
require 'vendor/autoload.php';

use Medoo\Medoo;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
//use PhpOffice\PhpSpreadsheet\Writer\Csv as CsvWrite;
use PhpOffice\PhpSpreadsheet\Reader\Csv as CsvRead;

$db = new Medoo([
    'database_type' => 'mysql',
    'database_name' => 'automateit',
    'server' => 'localhost',
    'username' => 'user',
    'password' => 'jayapura',
    'charset' => 'utf8'
]);

/*$spreadsheet = new Spreadsheet();
$sheet = $spreadsheet->getActiveSheet();
$sheet->setCellValue('A1', 'No');
$sheet->setCellValue('B1', 'Nama');
$sheet->setCellValue('A2', '1');
$sheet->setCellValue('B2', 'Lorem');

$writer = new  CsvWrite($spreadsheet);
$writer->setDelimiter(';');
$writer->setEnclosure('');
$writer->setLineEnding("\r\n");
$writer->setSheetIndex(0);
$writer->setUseBOM(true);
$writer->save('test.csv');*/

$basePath = '/var/www/html/automateit/webroot/files/csv/celebrities/';

while (true) {
    echo "File \033[34m" . basename(__FILE__, '.php') . "\033[0m Waiting for celebrit(y)ies to process on " . date('d-m-Y H:i') . PHP_EOL;
    $celebrities = $db->select('celebrities',
        [
            '[<]members' => ['member_id' => 'id'],
        ],
        [
            'celebrity' => [
                'celebrities.id(celebrity_id)',
                'celebrities.followers(celebrity_followers)',
                'celebrities.followersaved(celebrity_followersaved)',
            ],
            'member' => [
                'members.id(member_id)',
                'members.pk(pk)',
                'members.username(username)',
                'members.fullname(fullname)',
                'members.description(description)',
                'members.followings(followings)',
                'members.followers(followers)',
                'members.contents(contents)',
                'members.closed(closed)',
            ],
        ],
        [
            'AND' => [
                'celebrities.id[!]' => 1,
                'celebrities.active' => true
            ]
        ]
    );
   
    foreach ($celebrities as $celebrity) {
        $fullPath = $basePath . trim($celebrity['member']['username']) . '.csv';
        
        $fellowsCount = $db->count('vassals',
            [
                'AND' => [
                    'vassals.member_id' => $celebrity['member']['member_id'],
                    'vassals.active' => true,
                ]
            ]
        );

        // check if file exists
        $fileExists = file_exists($fullPath);
        // if no file, create
        if (!$fileExists && $fellowsCount > 0) {
            echo 'create file ' . $fullPath . PHP_EOL;
            createCsv($db, $celebrity, $fullPath, $fellowsCount);
        } else {
            // read csv row
            $csvCount = csvRowCount($fullPath);
            echo 'csv: ' . $csvCount . PHP_EOL;
            echo 'db: ' . $fellowsCount . PHP_EOL;

            if (($csvCount - 1) !== $fellowsCount && ($csvCount - 1) < $fellowsCount) {
                echo 'create file ' . $fullPath . PHP_EOL;
                createCsv($db, $celebrity, $fullPath, $fellowsCount);
            }
        }// .if file exists
    }// .foreach celebrities

    // sleep for a day
    sleep(86400);
}// .while true

function csvRowCount($fullPath = null) {
    //$reader = new CsvRead();
    //$spreadsheet = $reader->load($fullPath);
    //$rowCount = $spreadsheet->getActiveSheet()->getHighestDataRow();
    $fp = file($fullPath, FILE_SKIP_EMPTY_LINES);
    return count($fp);
    //return $rowCount;
    //echo $rowCount . PHP_EOL;
}// .csvRowCount

function readCsv($fullPath = null) {

}// .readCsv

function createCsv($db = null, $member = null, $fullPath = null, $fellowsCount = 0) {
    $data = [];
    $limit = 5000;
    $start = 0;
    do {
        echo $member['member']['username'] . ' load follower(s) data ' . $start . '/' . $fellowsCount . PHP_EOL;
        $fellows = $db->select('vassals',
            [
                '[<]fellows' => ['fellow_id' => 'id'],
            ],
            [
                'fellow' => [
                    'fellows.id(fellow_id)',
                    'fellows.pk(pk)',
                    'fellows.username(username)',
                    'fellows.fullname(fullname)',
                    'fellows.description(description)',
                    'fellows.followings(followings)',
                    'fellows.followers(followers)',
                    'fellows.contents(contents)',
                    'fellows.closed(closed)',
                ],
            ],
            [
                'AND' => [
                    'vassals.member_id' => $member['member']['member_id'],
                    'vassals.active' => true,
                ],
                'ORDER' => [
                    'fellows.username' => 'ASC'
                ],
                'LIMIT' => [
                    $start,
                    $limit
                ]
            ]
        );


       $handle = fopen($fullPath, 'a');
       if ($start == 0) {
            fputcsv($handle, [
                'No',
                'ID',
                'Username',
                'Fullname',
                'Private',
                'Link'],
                ';'
            );
        }

        $i = $start + 1;
        foreach ($fellows as $fellow) {
            $fellow['fellow']['closed'] ? $closed = 'YA' : $closed = 'TIDAK';
            $fellowFullname = preg_replace('/\s+/', ' ', $fellow['fellow']['fullname']);

            fputcsv($handle,[
                $start,
                $fellow['fellow']['pk'],
                $fellow['fellow']['username'],
                $fellowFullname,
                $closed,
                'https://www.instagram.com/' . $fellow['fellow']['username'],
            ],';');
            $i++;
            /*array_push($data, [
                $fellow['fellow']['pk'],
                $fellow['fellow']['username'],
                $fellowFullname,
                $closed,
                'https://www.instagram.com/' . $fellow['fellow']['username'],
            ]);*/
        }
        fclose($handle);
        $start = $start + $limit;
    } while ($start < $fellowsCount);
    

    //if (count($fellows) > 0) {
    $memberFullname = preg_replace('/\s+/', ' ', $member['member']['fullname']);
    //$memberDescription = preg_replace('/\s+/', ' ', $member['member']['description']);
    $member['member']['closed'] ? $closed = 'YA' : $closed = 'TIDAK';

    /*$spreadsheet = new Spreadsheet();
    $sheet = $spreadsheet->getActiveSheet();
    // column name
    /*$sheet->setCellValue('A1', 'No');
    $sheet->setCellValue('B1', 'ID');
    $sheet->setCellValue('C1', 'Username');
    $sheet->setCellValue('D1', 'Fullname');
    /*$sheet->setCellValue('D1', 'Biography');
    $sheet->setCellValue('E1', 'Followings');
    $sheet->setCellValue('F1', 'Followers');
    $sheet->setCellValue('G1', 'Posts');*/
    /*$sheet->setCellValue('E1', 'Private');
    // value
    $sheet->setCellValue('A2', 0);
    $sheet->setCellValue('B2', $member['member']['pk']);
    $sheet->setCellValue('C2', $member['member']['username']);
    $sheet->setCellValue('D2', $memberFullname);
    //$sheet->setCellValue('D2', $memberDescription);
    //$sheet->setCellValue('E2', $member['member']['followings']);
    //$sheet->setCellValue('F2', $member['member']['followers']);
    //$sheet->setCellValue('G2', $member['member']['contents']);
    $sheet->setCellValue('E2', $closed);*/
    // column name
    /*$sheet->setCellValue('A1', 'No');
    $sheet->setCellValue('B1', 'ID');
    $sheet->setCellValue('C1', 'Username');
    $sheet->setCellValue('D1', 'Fullname');
    /*$sheet->setCellValue('D3', 'Biography');
    $sheet->setCellValue('E3', 'Followings');
    $sheet->setCellValue('F3', 'Followers');
    $sheet->setCellValue('G3', 'Posts');*/
    /*$sheet->setCellValue('E1', 'Private');
    $sheet->setCellValue('F1', 'Link');

    $row = 2;
    $i = 1;
    foreach ($data as $fellow) {
        //foreach ($fellows as $fellow) {
        //$fellow['fellow']['closed'] ? $closed = 'YA' : $closed = 'TIDAK';
        //$fellowFullname = preg_replace('/\s+/', ' ', $fellow['fellow']['fullname']);
        //$fellowDescription = preg_replace('/\s+/', ' ', $fellow['fellow']['description']);

        $sheet->setCellValue('A' . $row, $i);
        $sheet->setCellValue('B' . $row, $fellow[0]);
        $sheet->setCellValue('C' . $row, $fellow[1]);
        $sheet->setCellValue('D' . $row, $fellow[2]);
        /*$sheet->setCellValue('D' . $row, $fellowDescription);
        $sheet->setCellValue('E' . $row, $fellow['fellow']['followings']);
        $sheet->setCellValue('F' . $row, $fellow['fellow']['followers']);
        $sheet->setCellValue('G' . $row, $fellow['fellow']['contents']);*/
        /*$sheet->setCellValue('E' . $row, $fellow[3]);
        $sheet->setCellValue('f' . $row, $fellow[4]);

        //echo $i . '. ' . $fellow['fellow']['username'] . PHP_EOL;
        $row++;
        $i++;
    }// .foreach fellows
    $writer = new CsvWrite($spreadsheet);
    $writer->setDelimiter(';');
    $writer->setEnclosure('');
    $writer->setLineEnding("\r\n");
    $writer->setSheetIndex(0);
    $writer->setUseBOM(true);
    $writer->save($fullPath);*/
    //}// .if count fellow > 0
}// .writeCsv