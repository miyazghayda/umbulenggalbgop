<?php
require 'vendor/autoload.php';

use Medoo\Medoo;
use InstagramAPI\Instagram;
use InstagramAPI\Constants;
use InstagramAPI\Signatures;
use InstagramAPI\Exception;

$db = new Medoo([
    'database_type' => 'mysql',
    'database_name' => 'automateit',
    'server' => 'database',
    'username' => 'user',
    'password' => 'jayapura',
    'charset' => 'utf8'
]);

// Get batch to process
$batch = 0;
if (isset($argv[1])) $batch = $argv[1];

while (true) {
    $ig = new Instagram(false, false);

    echo "File \033[34m" . basename(__FILE__, '.php') . "\033[0m Waiting for celebrit(y)ies to process on " . date('d-m-Y H:i') . PHP_EOL;
    $celebrities = $db->select('celebrities',
        [
            '[<]members' => ['member_id' => 'id'],
        ],
        [
            'celebrity' => [
                'celebrities.id(celebrity_id)',
                'celebrities.followers(celebrity_followers)',
                'celebrities.followersaved(celebrity_followersaved)',
                'celebrities.allfollowersaved(celebrity_allfollowersaved)',
                'celebrities.nextmaxid(celebrity_nextmaxid)'
            ],
            'member' => [
                'members.id(member_id)',
                'members.pk(member_pk)',
                'members.username(member_username)',
                'members.fullname(member_fullname)',
                'members.followers(member_followers)'
            ],
        ],
        [
            'AND' => [
                'celebrities.id[!]' => 1,
                'celebrities.allfollowersaved' => false,
                'celebrities.active' => true
            ]
        ]
    );
   
    foreach ($celebrities as $celebrity) {
        // Get all active account and pick one (random) to gather idol followers
        /*$accounts = $db->select('accounts',[
            '[<]proxies' => ['accounts.proxy_id' => 'id'],
        ],[
            'account' => [
                'accounts.id(account_id)',
                'accounts.proxy_id',
                'accounts.user_id',
                'accounts.username',
                'accounts.password',
            ],
            'proxy' => [
                'proxies.id(proxy_id)',
                'proxies.name(proxy_name)'
            ]
        ],[
            'AND' => [
                'accounts.statusid' => 5,
                'accounts.active' => true
            ]
        ]);*/
        $accounts = $db->select('accountlists',
                [
                    '[<]accounts' => ['account_id' => 'id'],
                    '[<]proxies' => ['accounts.proxy_id' => 'id'],
                ],
                [
                    'account' => [
                        'accounts.id(account_id)',
                        'accounts.proxy_id',
                        'accounts.username',
                        'accounts.password',
                    ],
                    'proxy' => [
                        'proxies.id(proxy_id)',
                        'proxies.name(proxy_name)',
                    ]
                ],
                [
                    'AND' => [
                        'accountlists.celebrity_id' => $celebrity['celebrity']['celebrity_id']
                    ]
                ]
        );
        $account = $accounts[0];
        //print_r($accounts);
        //$randomAccountIndex = rand(0, count($accounts) - 1);
        //$account = $accounts[$randomAccountIndex];

        // Login to ig
        try {
            if ($account['account']['proxy_id'] > 1) $ig->setProxy($account['proxy']['proxy_name']);
            echo 'Login with username ' . $account['account']['username'] . PHP_EOL;
            // Login akun IG
            $ig->login($account['account']['username'], $account['account']['password']);
        } catch (\Exception $e) {
            echo $e . PHP_EOL;
        }// .try login

        $followersaved = (int)$celebrity['celebrity']['celebrity_followersaved'];

        try {
            $rankToken = \InstagramAPI\Signatures::generateUUID();
            $maxId = $celebrity['celebrity']['celebrity_nextmaxid'];
            empty($maxId) ? $maxId = null : $maxId = $celebrity['celebrity']['celebrity_nextmaxid'];

            echo 'Now trying to gather ' . $celebrity['member']['member_username'] . ' followers' . PHP_EOL;
            $i = 0;
            do {
                $response = $ig->people->getFollowers($celebrity['member']['member_pk'], $rankToken, null, $maxId);
                if ($response->getStatus() === 'ok') {
                    foreach ($response->getUsers() as $u) {
                        // Insert Member
                        $fellow_id = insertMember($db, $u);
                        //array_push($fellowIds, $fellow_id);

                        // Insert Vassal
                        $vassal_id = insertVassal($db, $celebrity['member']['member_id'], $fellow_id);

                        echo $i . '. saving ' . $u->getUsername() . PHP_EOL;
                        $i++;
                    }
                }

                $maxId = $response->getNextMaxId();
                $followersaved = $followersaved + (int)$response->getPageSize();
                $db->update('celebrities',
                    ['nextmaxid' => $maxId, 'followersaved' => $followersaved],
                    ['id' => $celebrity['celebrity']['celebrity_id']]
                );
                
                // If end of followers
                if ($maxId === null) {
                    $db->update('celebrities',
                        ['allfollowersaved' => true],
                        ['id' => $celebrity['celebrity']['celebrity_id']]
                    );
                }

                // comment this on production
                // $maxId = null;
                sleep(rand(7, 12));
            } while ($maxId !== null);
        } catch (\Exception $e) {
            echo $e->getMessage() . PHP_EOL;
        }// .try gather followers


    }// .foreach accountlists
    
    sleep(1800);
}// .while true

function insertVassal($db = null, $member_id = 1, $fellow_id = 1) {
    $check = $db->select('vassals',
        ['id'],
        ['member_id' => $member_id, 'fellow_id' => $fellow_id, 'active' => true]
    );

    if (count($check) > 0) {
        return $check[0]['id'];
    } else {// save vassal (idol follower) to db
        $db->insert('vassals',[
            'member_id' => $member_id,
            'fellow_id' => $fellow_id,
            'created' => date('Y-m-d H:i:s'),
            'modified' => date('Y-m-d H:i:s'),
        ]);
        return $db->id();
    }
}// .function insertVassal

function insertMember($db = null, $datum = null) {
    $check = $db->select('members',
        ['id'],
        ['pk' => $datum->getPk(), 'active' => true]
    );

    if (count($check) > 0) {
        $db->update('members',
            ['closed' => $datum->getIsPrivate(), 'modified' => date('Y-m-d H:i:s')],
            ['id' => $check[0]['id']]);
        return $check[0]['id'];
    } else {// save member to db
        //echo "New member named " . $datum->getUsername() . "\n";
        ($datum->hasProfilePicUrl()) ? $profpicurl = $datum->getProfilePicUrl() : $profpicurl = '';

        $db->insert('members', 
            [
                'pk' => $datum->getPk(),
                'username' => $datum->getUsername(),
                'fullname' => $datum->getFullName(),
                'profpicurl' => $profpicurl,
                'closed' => $datum->getIsPrivate(),
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ]
        );
        return $db->id();
    }
}// .function insertMember