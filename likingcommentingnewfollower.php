<?php
require 'vendor/autoload.php';

use Medoo\Medoo;
use InstagramAPI\Instagram;
use InstagramAPI\Constants;
use InstagramAPI\Signatures;
use InstagramAPI\Exception;

require_once 'db.php';
/*$db = new Medoo([
    'database_type' => 'mysql',
    'database_name' => 'automateit',
    'server' => 'database',
    'username' => 'user',
    'password' => 'jayapura',
    'charset' => 'utf8'
]);*/

// Get batch to process
$batch = 0;
if (isset($argv[1])) $batch = $argv[1];

while (true) {
    $ig = new Instagram(false, false);
    echo "File \033[34m" . basename(__FILE__, '.php') . "\033[0m Waiting for account to process on " . date('d-m-Y H:i') . PHP_EOL;

    $accounts = $db->select('accounts',
        [
            '[>]preferences' => ['id' => 'account_id'],
            '[<]proxies' => ['proxy_id' => 'id'],
            '[<]members' => ['pk' => 'pk'],
        ],
        [
            'account' => [
                'accounts.id(account_id)',
                'accounts.proxy_id',
                'accounts.username',
                'accounts.password',
                'accounts.pk',
            ],
            'proxy' => [
                'proxies.id(proxy_id)',
                'proxies.name(proxy_name)',
            ],
            'preference' => [
                'preferences.id(preference_id)',
                'preferences.commentbynewfollowing',
                'preferences.maxlikeperday',
                'preferences.maxcommentperday',
                'preferences.liketoday',
                'preferences.commenttoday',
                'preferences.newfollowerposttolike',
                'preferences.newfollowerposttocomment',
            ],
            'member' => [
                'members.id(member_id)'
            ]
        ],
        [
            'AND' => [
                'accounts.statusid' => 5,
                'accounts.active' => true,
                'preferences.likebynewfollowing' => true
            ]
        ]);

        foreach ($accounts as $account) {
            $compositions = $db->select('compositions',
                ['caption'],
                [
                    'account_id' => $account['account']['account_id'],
                    'typeid' => 1,
                    'active' => true
                ]);
            $countCompositions = count($compositions);
               
            $yesterday = date('Y-m-d', strtotime(date('Y-m-d H:i:s') . ' -1 day'));
            $yesterday = $yesterday . ' 00:00:00';

            $followers = $db->select('vassals',
                [
                    '[<]members' => ['fellow_id' => 'id'],
                ],
                [
                    'vassal' => [
                        'vassals.id(vassal_id)',
                    ],
                    'member' => [
                        'members.id(member_id)',
                        'members.pk(member_pk)',
                        'members.username(member_username)',
                        'members.closed(member_closed)',
                    ]
                ],
                [
                    'AND' => [
                        'vassals.member_id' => $account['member']['member_id'],
                        'vassals.created[>]' => $yesterday,
                        'vassals.active' => true,
                    ]
            ]);

            if (count($followers) > 0) {
                try {
                    if ($account['account']['proxy_id'] > 1) $ig->setProxy($account['proxy']['proxy_name']);
                    echo 'Login with username ' . $account['account']['username'] . PHP_EOL;
                    // Login akun IG
                    $ig->login($account['account']['username'], $account['account']['password']);
                } catch (\Exception $e) {
                    echo $e . PHP_EOL;
                }// .try login

                $canComment = false;
                if ($countCompositions > 0 && $account['preference']['commentbynewfollowing']) $canComment = true;
                foreach ($followers as $follower) {
                    $needToLike = true;
                    $canView = true;
                    $canLike = true;

                    // check if follower got liked before, if yes don't process this follower
                    $checkFollower = $db->count('likinglists', [
                        'account_id' => $account['account']['account_id'],
                        'member_id' => $follower['member']['member_id'],
                        'active' => true
                    ]);
                    //echo $checkFollower . PHP_EOL;
                    if ($checkFollower > 0) $needToLike = false;

                    // if follower is private, check friendship first before gather post and liking
                    if ($follower['member']['member_closed'] == true) {
                        $respons = $ig->people->getFriendship($follower['member']['member_pk']);
                        if ($respons->getFollowing() == 0) $canView = false;
                    }// .if follower is private
                    
                    $likeToday = $account['preference']['liketoday'];
                    $maxLikePerDay = $account['preference']['maxlikeperday'];
                    $commentToday = $account['preference']['commenttoday'];
                    $maxCommentPerDay = $account['preference']['maxcommentperday'];
                    if ($likeToday >= $maxLikePerDay) $canLike = false;
                    if ($commentToday >= $maxCommentPerDay) $canComment = false;

                    if ($needToLike && $canView && ($canLike || $canComment)) {
                        $getContents = true;
                        try {
                            echo 'Get ' . $follower['member']['member_username'] . ' contents.' . PHP_EOL;
                            $maxId = null;
                            $mustLikingNPosts = $account['preference']['newfollowerposttolike'];
                            $mustCommentingNPosts = $account['preference']['newfollowerposttocomment'];
                            $hadLikingNPosts = 0;
                            $hadCommentingNPosts = 0;
                            do {
                                $response = $ig->timeline->getUserFeed($follower['member']['member_pk'], $maxId);
                                //print_r($response);
                                $user = $response->getItems()[0]->getUser();
                                $usernameComment = $user->getUsername();
                                $frontnameComment = $usernameComment;
                                $fullnameComment = $usernameComment;
                                if (!empty($user->getFullName())) {
                                    $fullnameComment = $user->getFullName();
                                    $name = explode(' ', $fullnameComment);
                                    $frontnameComment = $name[0];
                                }

                                foreach ($response->getItems() as $item) {
                                    // Insert Location
                                    $location_id = 1;
                                    $locationComment = '';
                                    if ($item->hasLocation() && $item->getLocation() !== null) {
                                        $location_id = insertLocation($db, $item->getLocation());
                                        $locationComment = $item->getLocation()->getName();
                                    }

                                    // Insert Post
                                    $post_id = insertPost($db, $item, $location_id, $follower['member']['member_id']);

                                    // Insert Wad
                                    if ($post_id > 0 && $item->getMediaType() == 1) {// Photo
                                        insertWad($db, $item->getImageVersions2()->getCandidates()[0], $post_id, $item->getMediaType());
                                    } elseif ($post_id > 0 && $item->getMediaType() == 2) {// Video
                                        insertWad($db, $item->getVideoVersions()[0], $post_id, $item->getMediaType());
                                    } elseif ($post_id > 0 && $item->getMediaType() == 8) {// Carousel
                                        insertWad($db, $item->getCarouselMedia(), $post_id, $item->getMediaType());
                                    }

                                    //($item->hasCaption() && $item->getCaption() !== null) ? $caption = $item->getCaption()->getText() : $caption = '';
                                    //echo 'Like/Comment Post with caption ' . $caption . PHP_EOL;
                                    $commentTemplate = $compositions[rand(0, ($countCompositions - 1))]['caption'];

                                    // liking
                                    if ($canLike && ($hadLikingNPosts < $mustLikingNPosts)) {
                                        //echo 'can like' . PHP_EOL;
                                        try {
                                            echo 'Liking post' . PHP_EOL;
                                            $ig->media->like($item->getId());
                                            insertLike($db, $account['account']['account_id'], $follower['member']['member_id'], $post_id);
                                            $hadLikingNPosts++;
                                            $likeToday++;
                                            sleep(rand(7, 12));
                                        } catch (\Exception $e) {
                                            echo $e . PHP_EOL;
                                        }// .try liking
                                        if ($likeToday >= $maxLikePerDay) $canLike = false;
                                    }// .if liking

                                    if ($canComment && ($hadCommentingNPosts < $mustCommentingNPosts)) {
                                        //echo 'can comment' . PHP_EOL;
                                        try {
                                            $comment = $commentTemplate;
                                            $comment = str_replace('@username', $usernameComment, $comment);
                                            $comment = str_replace('@fullname', $fullnameComment, $comment);
                                            $comment = str_replace('@frontname', $frontnameComment, $comment);
                                            if (!empty($locationComment)) {
                                                $comment = str_replace('@location', $locationComment, $comment);
                                            } else {
                                                $comment = str_replace('@location', '', $comment);
                                            }

                                            echo 'Comment with ' . $comment . PHP_EOL;
                                            $ig->media->comment($item->getId(), $comment);
                                            insertComment($db, $account['account']['account_id'], $follower['member']['member_id'], $post_id, $comment);
                                            $hadCommentingNPosts++;
                                            $commentToday++;
                                            //sleep(rand(7, 12));
                                        } catch (\Exception $e) {
                                            echo $e . PHP_EOL;
                                        }// try commenting
                                        if ($commentToday >= $maxCommentPerDay) $canComment = false;
                                    }// .if commenting
                                }

                                $maxId = $response->getNextMaxId();

                                if ($hadLikingNPosts >= $mustLikingNPosts) $getContents = false;
                                if ($hadCommentingNPosts >= $mustCommentingNPosts) $getContents = false;

                                $db->update('preferences',
                                    ['liketoday' => $likeToday, 'commenttoday' => $commentToday],
                                    ['account_id' => $account['account']['account_id']]);
                            } while($getContents);
                        } catch (\Exception $e) {
                            echo $e . PHP_EOL;
                        }// .try get post
                    }// .if can view this follower
                }// .foreach follower
            }// .if count compositions and count followers > 0

        }// .foreach account
    sleep(600);// sleep for 10 minutes
}// .while true

function insertLike($db = null, $account_id = 1, $member_id = 1, $post_id = 1) {
    $check = $db->select('commentinglists',
        ['id'],
        [
            'account_id' => $account_id,
            'member_id' => $member_id,
            'post_id' => $post_id,
            'active' => true
        ]
    );

    if (count($check) > 0) {
        return $check[0]['id'];
    } else {
        $db->insert('likinglists',
            [
                'account_id' => $account_id,
                'member_id' => $member_id,
                'post_id' => $post_id,
                'typeid' => 4,
                'liked' => true,
                'likedat' => date('Y-m-d H:i:s'),
                'who' => false,
                'note' => '',
            ]);
        return $db->id();
    }
}

function insertComment($db = null, $account_id = 1, $member_id = 1, $post_id = 1, $caption = null) {
    $check = $db->select('commentinglists',
        ['id'],
        [
            'account_id' => $account_id,
            'member_id' => $member_id,
            'post_id' => $post_id,
            'active' => true
        ]
    );

    if (count($check) > 0) {
        return $check[0]['id'];
    } else {
        $db->insert('commentinglists',
            [
                'account_id' => $account_id,
                'member_id' => $member_id,
                'post_id' => $post_id,
                'typeid' => 4,
                'commented' => true,
                'commentedat' => date('Y-m-d H:i:s'),
                'who' => false,
                'caption' => $caption,
                'note' => '',
            ]);
        return $db->id();
    }
}

function insertWad($db = null, $datum = null, $post_id = 1, $typeid = 1) {
    $newData = [
        'post_id' => $post_id,
        'typeid' => $typeid,
        'sequence' => 0
    ];

    if ($typeid == 1 || $typeid == 2) {// Single photo or video
        $newData['url'] = $datum->getUrl();
        $newData['width'] = $datum->getWidth();
        $newData['height'] = $datum->getHeight();
        $db->insert('wads', $newData);
        return $db->id();
    } elseif ($typeid == 8) {// Carousel
        $sequence = 0;
        foreach ($datum as $d) {
            $newData['url'] = '';
            $newData['width'] = 0;
            $newData['height'] = 0;
            $newData['typeid'] = $d->getMediaType();
            if ($d->getMediaType() == 1) {// Photo
                $reap = $d->getImageVersions2()->getCandidates()[0];
            } elseif ($d->getMediaType() == 2) {// Video
                $reap = $d->getVideoVersions()[0];
            }
            $newData['url'] = $reap->getUrl();
            $newData['width'] = $reap->getWidth();
            $newData['height'] = $reap->getHeight();
            $newData['sequence'] = $sequence;
            $sequence++;
            $db->insert('wads', $newData);
        }
    }
}// .insertWad

function insertPost($db = null, $datum = null, $location_id = 1, $member_id = 1) {
    $check = $db->select('posts',
        ['id'],
        ['pk' => $datum->getPk(), 'active' => true]
    );

    if (count($check) > 0) {
        return $check[0]['id'];
    } else {// save location to db
        ($datum->hasCaption() && $datum->getCaption() !== null) ? $caption = $datum->getCaption()->getText() : $caption = '';
        //echo "New post with caption $caption";

        $db->insert('posts', 
            [
                'pk' => $datum->getPk(),
                'sourceid' => $datum->getId(),
                'location_id' => $location_id,
                'member_id' => $member_id,
                'typeid' => $datum->getMediaType(),
                'caption' => $caption,
                'likes' => $datum->getLikeCount(),
                'comments' => $datum->getCommentCount(),
                'takenat' => $datum->getTakenAt()
            ]
        );
        return $db->id();
    }
}// .function insertPost

function insertMember($db = null, $datum = null) {
    $check = $db->select('members',
        ['id'],
        ['pk' => $datum->getPk(), 'active' => true]
    );

    if (count($check) > 0) {
        return $check[0]['id'];
    } else {// save location to db
        //echo "New member named " . $datum->getUsername() . "\n";
        ($datum->hasProfilePicUrl()) ? $profpicurl = $datum->getProfilePicUrl() : $profpicurl = '';

        $db->insert('members', 
            [
                'pk' => $datum->getPk(),
                'username' => $datum->getUsername(),
                'fullname' => $datum->getFullName(),
                'description' => $datum->getBiography(),
                'profpicurl' => $profpicurl,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            ]
        );
        return $db->id();
    }
}// .function insertMember

function insertLocation($db = null, $location = null) {
    $check = $db->select('locations',
        ['id'],
        ['pk' => $location->getPk(), 'active' => true]
    );

    if (count($check) > 0) {
        return $check[0]['id'];
    } else {// save location to db
        //echo "New location named " . $location->getName() . "\n";
        ($location->hasFacebookPlacesId()) ? $fbplacesid = $location->getFacebookPlacesId() : $fbplacesid = 0;

        $db->insert('locations', 
            [
                'pk' => $location->getPk(),
                'lat' => $location->getLat(),
                'lng' => $location->getLng(),
                'shortname' => $location->getShortName(),
                'name' => $location->getName(),
                'address' => $location->getAddress(),
                'fbplacesid' => $fbplacesid

            ]
        );
        return $db->id();
    }
}// .function insertLocation
