<?php
require 'vendor/autoload.php';

use Medoo\Medoo;
use InstagramAPI\Instagram;
use InstagramAPI\Constants;
use InstagramAPI\Signatures;
use InstagramAPI\Exception;

$db = new Medoo([
    'database_type' => 'mysql',
    'database_name' => 'automateit',
    'server' => 'localhost',
    'username' => 'user',
    'password' => 'jayapura',
    'charset' => 'utf8'
]);

// Get batch to process
$batch = 0;
if (isset($argv[1])) $batch = $argv[1];

while (true) {
    echo "File \033[34m" . basename(__FILE__, '.php') . "\033[0m Waiting for account(s) to process on " . date('d-m-Y H:i') . PHP_EOL;

    $accountsOnFollowinglist = $db->select('followinglists',
    ['account_id'],
    [
        'AND' => [
            'typeid' => 1,
            'followed' => false,
            'unfollowed' => false,
            'active' => true
        ],
        'GROUP' => ['account_id']
    ]);

    // Remove redundant account_id
    $accountIds = [];
    foreach ($accountsOnFollowinglist as $a) {
        if (!in_array($a['account_id'], $accountIds)) array_push($accountIds, $a['account_id']);
    }
    // Check if account_id is paid account
    $paidAccountIds = [];
    foreach ($accountIds as $a) {
        $check = $db->count('accounts',
        [
            'AND' => [
                'id' => $a,
                'statusid' => 5,
                'active' => true,
            ]
        ]);
        if ($check > 0) {
            array_push($paidAccountIds, $a);
        }
    }

    if (count($paidAccountIds) > 0) {
        $ig = new Instagram(false, false);
        foreach ($paidAccountIds as $account_id) {
            $account = $db->select('accounts',
                [
                    '[<]proxies' => ['proxy_id' => 'id'],
                    '[>]preferences' => ['id' => 'account_id'],
                ],
                [
                    'account' => [
                        'accounts.id(account_id)',
                        'accounts.user_id',
                        'accounts.username',
                        'accounts.password',
                        'accounts.proxy_id'
                    ],
                    'proxy' => [
                        'proxies.name AS proxy_name',
                    ],
                    'preference' => [
                        'preferences.maxfollowperday',
                        'preferences.followidolfollower',
                        'preferences.followbyhashtag',
                        'preferences.followbylocation',
                        'preferences.followtoday',
                        'preferences.idolfollowertofollowtoday',
                        'preferences.getidolfollowertofollowtoday',
                    ],
                ],
                [
                    'AND' => [
                        'accounts.id' => $account_id,
                        'preferences.followbatch' => $batch,
                        'preferences.followidolfollower' => true,
                        'preferences.maxfollowperday[>]' => 0,
                        'preferences.getidolfollowertofollowtoday' => true
                    ],
                    'LIMIT' => 1
            ]);
            if (count($account) > 0) {
                $account = $account[0];

                $maxFollowTodayDivider = 1;
                if ($account['preference']['followbyhashtag']) $maxFollowTodayDivider++;
                if ($account['preference']['followbylocation']) $maxFollowTodayDivider++;

                $maxFollowPerDay = $account['preference']['maxfollowperday'];

                $maxFollowPerDayPerMode = floor($maxFollowPerDay / $maxFollowTodayDivider);
                $followToday = $account['preference']['followtoday'];
                
                // different per mode
                $followTodayIdolFollower = $account['preference']['idolfollowertofollowtoday'];
                $toFollowNow = $maxFollowPerDayPerMode - $followTodayIdolFollower;

                if ($followToday <= $maxFollowPerDay && $followTodayIdolFollower <= $maxFollowPerDayPerMode) {
                    try {
                        if ($account['account']['proxy_id'] > 1) $ig->setProxy($account['account']['proxy']['proxy_name']);
                        echo 'Login with username ' . $account['account']['username'] . PHP_EOL;
                        // Login akun IG
                        $ig->login($account['account']['username'], $account['account']['password']);

                        // Process following idol follower
                        $followinglists = $db->select('followinglists',
                        [
                            '[<]members' => ['member_id' => 'id'],
                            '[<]fellows' => ['fellow_id' => 'id'],
                        ],
                        [
                            'followinglist' => [
                                'followinglists.id(followinglist_id)',
                            ],
                            'member' => [
                                'members.pk(member_pk)',
                                'members.username(member_username)',
                                'members.fullname(member_fullname)',
                            ],
                            'fellow' => [
                                'fellows.pk(fellow_pk)',
                                'fellows.username(fellow_username)',
                                'fellows.fullname(fellow_fullname)',
                            ]
                        ],
                        [
                            'AND' => [
                                'followinglists.account_id' => $account['account']['account_id'],
                                'followinglists.followed' => false,
                                'followinglists.unfollowed' => false,
                                'followinglists.active' => true,
                            ],
                            'LIMIT' => $toFollowNow
                        ]);


                        foreach ($followinglists as $f) {
                            try {
                                $ig->people->follow($f['fellow']['fellow_pk']);
                                $db->update('followinglists',[
                                    'followed' => true,
                                    'followedat' => date('Y-m-d H:i:s'),
                                ],
                                [
                                    'id' => $f['followinglist']['followinglist_id']
                                ]);
                                echo $account['account']['username'] . ' now following ' . $f['fellow']['fellow_username'] .
                                ' (follower of ' . $f['member']['member_username'] . ')' . PHP_EOL;
                            } catch (\Exception $e) {
                                echo $e->getMessage() . PHP_EOL;
                            }
                            sleep(rand(28, 38));
                        }// .foreach followinglists

                        // update preferences table
                        $db->update('preferences', [
                            'followtoday' => $followToday + $toFollowNow,
                            'idolfollowertofollowtoday' => $toFollowNow,
                            'getidolfollowertofollowtoday' => false
                        ],
                        [
                            'account_id' => $account['account']['account_id']
                        ]);
                    } catch (\Exception $loginException) {
                        echo $loginException->getMessage() . PHP_EOL;
                    }// try login ig
                }// .if follow counter is below maximum
            }// .if there's account to process
        }// .foreach paid account
    }// .if count paid account > 0
     
    // sleep foreach process
    sleep(1800);
}// .while true 